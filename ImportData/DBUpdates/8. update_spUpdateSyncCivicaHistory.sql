USE [Rotunda]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateSyncCivicaHistory]    Script Date: 4/12/2022 4:10:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spUpdateSyncCivicaHistory] (
	@Id bigint,
	@TypeSync varchar(5),
	@Dt varchar(20),
	@Dur int,
	@Message nvarchar(500),
	@Status bit,
	@StatusUpdate bit,
	@UpdateTime DateTime
) AS
BEGIN
	--check exist
	declare @checkItem int = 0;
	select @checkItem = COUNT(*) from HistorySyncCivica where dt = @Dt and Dur = @Dur 
	If(@checkItem > 0)
	begin
		update HistorySyncCivica
		set TypeSync=@TypeSync
			,Dt=@Dt
			,Dur=@Dur
			,Message=@Message
			,Status=@Status
			,StatusUpdate=@StatusUpdate
			,UpdatedTime=@UpdateTime
		 where dt = @Dt and Dur = @Dur 
	end
	else
	begin
	 INSERT INTO [dbo].[HistorySyncCivica]
           ([TypeSync]
           ,[Dt]
           ,[Dur]
           ,[Message]
           ,[Status]
           ,[StatusUpdate]
           ,[CreatedTime]
           ,[UpdatedTime])
     VALUES
           (@TypeSync
			,@Dt
			,@Dur
			,@Message
			,@Status
			,@StatusUpdate
			,@UpdateTime
			,NULL)
	end
END

