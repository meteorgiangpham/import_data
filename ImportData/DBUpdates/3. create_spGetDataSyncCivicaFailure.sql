USE [Rotunda]
GO
/****** Object:  StoredProcedure [dbo].[spFAQsPublish]    Script Date: 2/18/2022 2:10:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetDataSyncCivicaFailure] (	
	@TypeSync varchar(5)
) AS
BEGIN
	 select * from HistorySyncCivica where TypeSync = @TypeSync and (Status = 0 or (StatusUpdate is not null and StatusUpdate = 0))
END

