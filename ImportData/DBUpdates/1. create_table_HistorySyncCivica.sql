USE [Rotunda]
GO

/****** Object:  Table [dbo].[HistorySyncCivica]    Script Date: 2/24/2022 10:32:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HistorySyncCivica](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TypeSync] [varchar](5) NULL,
	[Dt] [varchar](20) NULL,
	[Dur] [int] NULL,
	[Message] [nvarchar](500) NULL,
	[Status] [bit] NULL,
	[StatusUpdate] [bit] NULL,
	[CreatedTime] [datetime] NULL,
	[UpdatedTime] [datetime] NULL,
 CONSTRAINT [PK_HistorySyncCivica] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


