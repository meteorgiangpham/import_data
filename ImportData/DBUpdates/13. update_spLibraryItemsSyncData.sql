USE [Rotunda]
GO
/****** Object:  StoredProcedure [dbo].[spLibraryItemsSyncData]    Script Date: 6/7/2022 10:21:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spLibraryItemsSyncData] (	
	@LibraryItem_IRN varchar(200),
	@LibraryItem_BibIrn varchar(200),
	@LibraryItem_LibraryId  int ,
	@LibraryItem_Barcode  varchar(50) ,
	@LibraryItem_BRN  varchar(50) ,
	@LibraryItem_Location  nvarchar(max) ,
	@LibraryItem_Collection  nvarchar(300) ,
	@LibraryItem_Category1  nvarchar(300) ,
	@LibraryItem_Category2  nvarchar(300) ,
	@LibraryItem_ClassNumber  varchar(50) ,
	@LibraryItem_Suffix  varchar(20) ,
	@LibraryItem_Prefix  varchar(20) ,
	@LibraryItem_Status  varchar(50) ,
	@LibraryItem_StatusDescription  nvarchar(500) ,
	@LibraryItem_StatusDate  datetime ,
	@LibraryItem_Availability  varchar(10) ,
	@LibraryItem_IsActive  bit ,
	@LibraryItem_CreatedDate  datetime ,
	@LibraryItem_ModifiedDate  datetime
) AS
BEGIN
			--check delete item
			if(@LibraryItem_Status = 'Missing' or @LibraryItem_Collection = 'Auction Catalogues' or @LibraryItem_Collection = 'Publications Reserve' or @LibraryItem_Collection = 'Digitised Collection')
			begin
				set @LibraryItem_IsActive = 0;
			end
			--select libraryId
			DECLARE @LibraryId INT
			SELECT @LibraryId=Library_Id FROM Library WHERE Library_BRN = @LibraryItem_BRN
			--check exist item
			declare @checkItem int = 0;
			select @checkItem = COUNT(*) from LibraryItems where LibraryItem_Barcode = @LibraryItem_Barcode
			if(@checkItem > 0)
			begin
				UPDATE LibraryItems
				SET LibraryItem_Barcode = ISNULL(@LibraryItem_Barcode, LibraryItem_Barcode)
					,LibraryItem_Location = ISNULL(@LibraryItem_Location, LibraryItem_Location)                
					,LibraryItem_Collection = ISNULL(@LibraryItem_Collection, LibraryItem_Collection)
					,LibraryItem_Category1 = ISNULL(@LibraryItem_Category1, LibraryItem_Category1)                    
					,LibraryItem_Category2 = ISNULL(@LibraryItem_Category2, LibraryItem_Category2)                  
					,LibraryItem_ClassNumber = ISNULL(@LibraryItem_ClassNumber, LibraryItem_ClassNumber)                  
					,LibraryItem_Suffix = ISNULL(@LibraryItem_Suffix, LibraryItem_Suffix)                  
					,LibraryItem_Prefix = ISNULL(@LibraryItem_Prefix, LibraryItem_Prefix)                  
					,LibraryItem_Status = ISNULL(@LibraryItem_Status, LibraryItem_Status)                  
					,LibraryItem_StatusDescription = ISNULL(@LibraryItem_StatusDescription, LibraryItem_StatusDescription)                    
					,LibraryItem_StatusDate = ISNULL(@LibraryItem_StatusDate, LibraryItem_StatusDate)               
					,LibraryItem_Availability = ISNULL(@LibraryItem_Availability, LibraryItem_Availability)                  
					,LibraryItem_IsActive = ISNULL(@LibraryItem_IsActive, LibraryItem_IsActive)          
					,LibraryItem_CreatedDate = ISNULL(@LibraryItem_CreatedDate, LibraryItem_CreatedDate)               
					,LibraryItem_ModifiedDate = ISNULL(@LibraryItem_ModifiedDate, LibraryItem_ModifiedDate)
					,LibraryItem_IRN = ISNULL(@LibraryItem_IRN, LibraryItem_IRN)
					,LibraryItem_LibraryId = ISNULL(@LibraryId, LibraryItem_LibraryId)
				WHERE LibraryItem_Barcode = @LibraryItem_Barcode
			end
			else
			begin
				INSERT INTO [dbo].[LibraryItems]
					   ([LibraryItem_LibraryId]
					   ,[LibraryItem_IRN]
					   ,[LibraryItem_Barcode]
					   ,[LibraryItem_BRN]
					   ,[LibraryItem_Location]
					   ,[LibraryItem_Collection]
					   ,[LibraryItem_Category1]
					   ,[LibraryItem_Category2]
					   ,[LibraryItem_ClassNumber]
					   ,[LibraryItem_Suffix]
					   ,[LibraryItem_Prefix]
					   ,[LibraryItem_Status]
					   ,[LibraryItem_StatusDescription]
					   ,[LibraryItem_StatusDate]
					   ,[LibraryItem_Availability]
					   ,[LibraryItem_IsActive]
					   ,[LibraryItem_CreatedDate]
					   ,[LibraryItem_ModifiedDate])
				 VALUES
					   (@LibraryId  
					    ,@LibraryItem_IRN
						,@LibraryItem_Barcode                  
						,@LibraryItem_BRN                  
						,@LibraryItem_Location                    
						,@LibraryItem_Collection                    
						,@LibraryItem_Category1                    
						,@LibraryItem_Category2                    
						,@LibraryItem_ClassNumber                  
						,@LibraryItem_Suffix                  
						,@LibraryItem_Prefix                  
						,@LibraryItem_Status                  
						,@LibraryItem_StatusDescription                    
						,@LibraryItem_StatusDate               
						,@LibraryItem_Availability                  
						,@LibraryItem_IsActive          
						,@LibraryItem_CreatedDate               
						,@LibraryItem_ModifiedDate)
			end
		
END

