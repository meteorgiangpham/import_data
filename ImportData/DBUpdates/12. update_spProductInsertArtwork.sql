USE [Rotunda]
GO
/****** Object:  StoredProcedure [dbo].[spProductInsertArtwork]    Script Date: 5/25/2022 9:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spProductInsertArtwork] (	
	@UMOId				VARCHAR(20),
	@FilePath			VARCHAR(MAX),
	@ImagePath			VARCHAR(MAX),
	@Title				NVARCHAR(MAX),
	@Artist				NVARCHAR(500),
	@ProductDate		VARCHAR(200),
	@ProductLocation	NVARCHAR(MAX),
	@AccessLevel		VARCHAR(50),
	@SystemId			INT,
	@AccessionNo		VARCHAR(200)=NULL,	
	@ArtistDateOfBirth	VARCHAR(50),
	@ArtistDateOfDeath	VARCHAR(50),
	@Creditline 		VARCHAR(300),
	@Medium 			VARCHAR(200),
	@Dimensions			VARCHAR(300),	
	@OnDisplay			VARCHAR(50),
	@PhotoCredit		VARCHAR(200),
	@Genere				VARCHAR(200)=NULL,
	@Copyright			VARCHAR(200)=NULL,
	@ExhibitionHistory		NVARCHAR(MAX),	
	@DisplayLocation		VARCHAR(50)=NULL,
	@Remarks			NVARCHAR(MAX),
	@Description			NVARCHAR(MAX)

)
AS
BEGIN

DECLARE @ProductId INT
SELECT @ProductId=Artwork_ProductID FROM Artwork WHERE Artwork_UMO_Id = @UMOId


DECLARE @Active BIT
SET  @Active= CASE WHEN @Title ='' THEN 0 
				WHEN @Artist ='' THEN 0 				
				ELSE  1 END  -- if title or artist is blank, product will be nonactive


IF ISNULL(@ProductId,0)>0
BEGIN

UPDATE Products
		SET
			Product_FilePath =ISNULL(@FilePath,Product_FilePath),
			Product_ImagePath =ISNULL(@ImagePath,Product_ImagePath),			
			Product_Title =ISNULL(@Title,Product_Title), 
			Product_Artist =ISNULL(@Artist,Product_Artist), 
			Product_ProductDate =ISNULL(@ProductDate,Product_ProductDate),
			Product_Location =ISNULL(@ProductLocation,Product_Location),
			Product_AccessLevel =ISNULL(@AccessLevel,Product_AccessLevel),
			Product_IsActive=ISNULL(@Active,Product_IsActive),
			Product_ModifiedDate=GETDATE()	
		WHERE Product_Id=@ProductId



UPDATE Artwork
		SET Artwork_AccessionNo = ISNULL(@AccessionNo,Artwork_AccessionNo),
			Artwork_ArtistDateOfBirth =ISNULL(@ArtistDateOfBirth,Artwork_ArtistDateOfBirth), 
			Artwork_ArtistDateOfDeath =ISNULL(@ArtistDateOfDeath,Artwork_ArtistDateOfDeath),
			Artwork_Creditline =ISNULL(@Creditline,Artwork_Creditline),
			Artwork_Medium =ISNULL(@Medium,Artwork_Medium),
			Artwork_Dimensions =ISNULL(@Dimensions,Artwork_Dimensions),
			Artwork_OnDisplay =ISNULL(@OnDisplay,Artwork_OnDisplay),
			Artwork_PhotoCredit =ISNULL(@PhotoCredit,Artwork_PhotoCredit),
			Artwork_Genere =ISNULL(@Genere,Artwork_Genere),			
			Artwork_Copyright =ISNULL(@Copyright,Artwork_Copyright),
			Artwork_ExhibitionHistory =ISNULL(@ExhibitionHistory,Artwork_ExhibitionHistory),
			Artwork_DisplayLocation =ISNULL(@DisplayLocation,Artwork_DisplayLocation),
			Artwork_Remarks =ISNULL(@Remarks,Artwork_Remarks),
			Artwork_Description = ISNULL(@Description, Artwork_Description)
		WHERE Artwork_UMO_Id=@UMOId

--Update Records
--do what you need if exists
END
ELSE
BEGIN



	INSERT INTO Products(Product_CategoryCode,Product_FilePath,Product_ImagePath,Product_Title,Product_Artist,Product_ProductDate,Product_Location,Product_AccessLevel,Product_UMO_Id,Product_IsActive)
	VALUES('ART',@FilePath,@ImagePath,@Title,@Artist,@ProductDate,@ProductLocation,@AccessLevel,@UMOId,@Active)

	SELECT  @ProductId=SCOPE_IDENTITY()


	INSERT INTO Artwork(Artwork_UMO_Id,Artwork_ProductId,Artwork_SystemId,Artwork_AccessionNo,Artwork_ArtistDateOfBirth,Artwork_ArtistDateOfDeath
	,Artwork_Creditline,Artwork_Medium,Artwork_Dimensions,Artwork_OnDisplay,Artwork_PhotoCredit,Artwork_Genere,Artwork_Copyright,Artwork_ExhibitionHistory,Artwork_DisplayLocation,Artwork_Remarks, Artwork_Description)
	VALUES(@UMOId,@ProductId,@SystemId,@AccessionNo,@ArtistDateOfBirth,@ArtistDateOfDeath
	,@Creditline,@Medium,@Dimensions,@OnDisplay,@PhotoCredit,@Genere,@Copyright,@ExhibitionHistory,@DisplayLocation,@Remarks,@Description)

--do what needs to be done if not
END
	
	

END

