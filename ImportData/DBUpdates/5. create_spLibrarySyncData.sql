USE [Rotunda]
GO
/****** Object:  StoredProcedure [dbo].[spFAQsPublish]    Script Date: 2/18/2022 2:10:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spLibrarySyncData] (	
	@Library_IRN  varchar(200),
	@Library_BRN  varchar(300),
	@Library_ISBN  varchar(50),
	@Library_ISSN  varchar(50),
	@Library_Title  nvarchar(max),
	@Library_MeetingName  nvarchar(max),
	@Library_EditionStatement  nvarchar(max),
	@Library_Summary  nvarchar(max),
	@Library_DeweyClass  varchar(50),
	@Library_Language  varchar(200),
	@Library_Created  datetime,
	@Library_LastUpdated  datetime,
	@Library_LinkingNotes  nvarchar(max),
	@Library_HostItemEntry  varchar(300)
) AS
BEGIN
	BEGIN TRY  
		BEGIN TRANSACTION
			declare @checkItem int = 0;
			--check exist item
			select @checkItem = COUNT(*) from Library where Library_IRN = @Library_IRN
			if(@checkItem > 0)
			begin
				UPDATE Library
				SET Library_BRN=@Library_BRN         
					,Library_ISBN=@Library_ISBN        
					,Library_ISSN=@Library_ISSN        
					,Library_Title=@Library_Title              
					,Library_MeetingName=@Library_MeetingName              
					,Library_EditionStatement=@Library_EditionStatement              
					,Library_Summary=@Library_Summary              
					,Library_DeweyClass=@Library_DeweyClass        
					,Library_Language=@Library_Language         
					,Library_Created=@Library_Created              
					,Library_LastUpdated=@Library_LastUpdated              
					,Library_LinkingNotes=@Library_LinkingNotes              
					,Library_HostItemEntry=@Library_HostItemEntry 
				WHERE Library_IRN = @Library_IRN
			end
			else
			begin
				INSERT INTO [dbo].[Library]
					   ([Library_IRN]                           
						,[Library_BRN]         
						,[Library_ISBN]        
						,[Library_ISSN]        
						,[Library_Title]              
						,[Library_MeetingName]              
						,[Library_EditionStatement]              
						,[Library_Summary]              
						,[Library_DeweyClass]        
						,[Library_Language]         
						,[Library_Created]              
						,[Library_LastUpdated]              
						,[Library_LinkingNotes]              
						,[Library_HostItemEntry]  )
				 VALUES
					   (@Library_IRN
						,@Library_BRN
						,@Library_ISBN
						,@Library_ISSN
						,@Library_Title
						,@Library_MeetingName
						,@Library_EditionStatement
						,@Library_Summary
						,@Library_DeweyClass
						,@Library_Language
						,@Library_Created
						,@Library_LastUpdated
						,@Library_LinkingNotes
						,@Library_HostItemEntry)
			end
		COMMIT TRANSACTION
	END TRY  
	BEGIN CATCH  
		ROLLBACK TRANSACTION
	END CATCH  
END

