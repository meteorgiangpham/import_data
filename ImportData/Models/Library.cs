﻿using ImportData.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportData
{
    public class LibraryItems
    {
        public string IRN;
        public string BRN;
        public string BibIrn;
        public string Barcode;
        public string Location;
        public string Category1;
        public string Category2;
        public string ClassNumber;

        public string Suffix;
        public string Prefix;
        public string ItemStatus;
        public string StatusSetDate;
        public string Availability;
        public string CollectionCode;
        public string Description;

        public DateTime? StatusDate
        {
            get
            {
                try { return Convert.ToDateTime(StatusSetDate); }
                catch { return null; }
            }
        }

        //[JsonIgnore()]
        //public DateTime? StatusDate
        //{
        //    get
        //    {
        //        try { return DateTime.ParseExact(StatusSetDate, "dd/MM/yyyyTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); }
        //        catch { return null; }
        //    }
        //}

    }


    public class LibraryBIB
    {
        //DateTime.ParseExact(HttpContext.Current.Request["TXN_DATE_TIME"], "dd/MM/yyyyTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),

    
        public string IRN;
        public string BRN;
        public string ISBN;
        public string ISSN;        
        public string Title;

        public string RemainderofTitle;
        public string StatementofResponsibility;
        public string Medium;
        public string VaryingformofTitle;
        public string RemainderofTitle2;
        public string MainEntryPersonalName;
        public string MainEntryCorporateName;
        public string MeetingName;

        public string EditionStatement;
        public string PlaceofPublication;
        public string NameofPublisher;


        public string DateofPublication;
        public string Collation_Extent;
        public string Collation_OtherPhysicalDetails;


        public string Collation_Dimensions;
        public string Collation_Accompanying;

   

        public string SeriesStatement_Title;
        public string SeriesStatement_Volumenumber;
        public string GeneralNote;
        public string FormattedContentsNote;
        public string Summary;
        public string LanguageNote;
        public string SubjectAddedEntry_TopicalTerm;
        public string SubjectAddedEntry_PersonalName;
        public string SubjectAddedEntry_CorporateName;

        public string SubjectAddedEntry_MeetingName;

        public string AddedEntry_PersonalName;
        public string AddedEntry_CorporateName;
        public string AddedEntry_MeetingName;
      
        public string Location;
        public string SublocationorCollection;
        public string Classificationpart;
        public string CallnumberSuffix;
        public string Publicnote;
        public string DeweyClass;
    
        public string Language;
        public string Country;
        public string CreatedDateTime;
        public string LastUpdatedDateTime;
        public string Linkingnotes_PrecedingEntry;
        public string Linkingnotes_SucceedingEntry;
        public string HostItemEntry;
        public string AwardsNote;

        public string CreationorProductionCreditsNote;
        public string ParticipantorPerformerNote;
        public string SystemDetailsNote;
        //public string BookCover;
        public string BookCoverMedium;
        public string BookCoverSmall;
        public string BookCoverLarge;


        [JsonIgnore()]
        public DateTime? CreatedDate
        {
            get
            {                 
                try
                {
                    DateTime dt = DateTime.ParseExact(CreatedDateTime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                    return dt;
                    //return Convert.ToDateTime(CreatedDateTime);
                }
                catch (Exception e)
                {
                    LogService.CreateErrorLog("Library CreatedDate format error. Detail error info : " + e.ToString());
                    return null;
                }
            }
        }

        [JsonIgnore()]
        public DateTime? UpdatedDate
        {
            get
            {
                try
                {
                    DateTime dt = DateTime.ParseExact(LastUpdatedDateTime, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                    return dt;

                    //return Convert.ToDateTime(LastUpdatedDateTime);
                }
                catch(Exception e)
                {
                    LogService.CreateErrorLog("Library LastUpdatedDateTime format error. Detail error info : " + e.ToString());
                    return null;
                }
            }
        }

     


    }

    public class LibraryBIBFile
    {
        public List<LibraryBIB> ItemData;
    }



    //public class LibraryItemFile
    //{
    //    public List<LibraryItem> ItemData;
    //}

}
