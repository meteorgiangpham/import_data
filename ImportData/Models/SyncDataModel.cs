﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportData.Models
{
    public class SyncResponse
    {
        public bool status;
        public string message;
    }

    public class SyncDataBiBModel
    {
        public long idSync;
        public string dt;
    }

    public class SyncDataItemModel
    {
        public long idSync;
        public string dt;
        public int dur;
    }

    public class SyncDataResponse
    {
        public string status;
        public string message;
    }

    public class SyncBiBListData
    {
        public string status;
        public string msg;
        public List<BiBItem> data;
    }

    public class BiBItem
    {
        public double irn;
    }

    public class SyncBiBDetailData
    {
        public string status;
        public string msg;
        public BiBDetail data;
    }

    public class BiBDetail
    {
        public int IRN;
        public string BRN;
        public string ISBN;
        public string ISSN;
        public string Title;
        public string RemainderofTitle;
        public string StatementofResponsibility;
        public string Medium;
        public string VaryingformofTitle;
        public string RemainderofTitle2;
        public string MainEntryPersonalName;
        public string MainEntryCorporateName;
        public string MeetingName;
        public string EditionStatement;
        public string PlaceofPublication;
        public string NameofPublisher;
        public string DateofPublication;
        public string Collation_Extent;
        public string Collation_OtherPhysicalDetails;
        public string Collation_Dimensions;
        public string Collation_Accompanying;
        public string SeriesStatement_Title;
        public string SeriesStatement_Volumenumber;
        public string GeneralNote;
        public string FormattedContentsNote;
        public string Summary;
        public string LanguageNote;
        public string SubjectAddedEntry_TopicalTerm;
        public string SubjectAddedEntry_PersonalName;
        public string SubjectAddedEntry_CorporateName;
        public string SubjectAddedEntry_MeetingName;
        public string AddedEntry_PersonalName;
        public string AddedEntry_CorporateName;
        public string AddedEntry_MeetingName;
        public string Location;
        public string SublocationorCollection;
        public string Classificationpart;
        public string CallnumberSuffix;
        public string Publicnote;
        public string DeweyClass;
        public string Language;
        public string Country;
        public string CreatedDateTime;
        public string LastUpdatedDateTime;
        public string DeactivatedDateTime;
        public string Linkingnotes_PrecedingEntry;
        public string Linkingnotes_SucceedingEntry;
        public string HostItemEntry;
        public string AwardsNote;
        public string CreationorProductionCreditsNote;
        public string ParticipantorPerformerNote;
        public string SystemDetailsNote;
        public string BookCoverSmall;
        public string BookCoverMedium;
        public string BookCoverLarge;
        public int TotalHoldings;
    }

    public class SyncItemListData
    {
        public string status;
        public string msg;
        public List<Item> data;
    }

    public class Item
    {
        public double irn;
    }

    public class SyncItemDetailData
    {
        public string status;
        public string msg;
        public ItemDetail data;
    }

    public class ItemDetail
    {
        public int IRN;
        public string BRN;
        public string BibIrn;
        public string Barcode;
        public string Location;
        public string Category1;
        public string Category2;
        public string ClassNumber;
        public string Suffix;
        public string Prefix;
        public string ItemStatus;
        public string StatusSetDate;
        public string Availability;
        public string CollectionCode;
        public string Description;
        public string CreatedDateTime;
        public string LastUpdatedDateTime;
        public string DeactivatedDateTime;
    }

    public class SyncCivica
    {
        public long Id;
        public string TypeSync;
        public string Dt;
        public int Dur;
        public string Message;
        public bool Status;
        public bool StatusUpdate;
        public DateTime UpdateTime;
    }

    public class LoginCivicaResponse
    {
        public string access_token;
        public string token_type;
        public double expires_in;
    }
}
