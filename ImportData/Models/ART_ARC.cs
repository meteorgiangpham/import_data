﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportData
{

    public class DeletedUMOs
    {
        public UMOs[] deleted;        

    }
    public class UMOs
    {
        public string umo;
        public string dc;      
        //{"umo":"12265888","dc":"30-Jul-2019 18:24","aid":"8825697","cname":"1. Explorer Tours Data"} 

    }


    public class ART_ARC
    {
        public string umo_id;
        public string date_updated;
        public string image_type;// to define image or VIDEO
        public DateTime? UpdatedDate
        {
            get
            {
                try { return Convert.ToDateTime(date_updated); }
                catch { return null; }
            }
        }

        public Derivs[] derivs;
        public Thumbnail[] thumbnail;
        public MetaData metadata;

    }

    public class MetaData
    {
        #region ArtWork
        [JsonProperty(PropertyName = "ARTWORK DATA.ACCESSIONNUMBER")]
        public string ACCESSIONNUMBER;
        ////[JsonProperty(PropertyName = "ARTWORK DATA.ARTIST")]
        ////public string ARTIST;

        [JsonProperty(PropertyName = "ARTWORK DATA.ARTIST")]
        public ARTIST_INFO[] ARTISTS;

        [JsonProperty(PropertyName = "ARTWORK DATA.ARTIST_BIRTHDATE")]
        public string ARTIST_BIRTHDATE;

        [JsonProperty(PropertyName = "ARTWORK DATA.ARTIST_DEATH_DATE")]
        public string ARTIST_DEATH_DATE;

        [JsonProperty(PropertyName = "ARTWORK DATA.CAPTION")]
        public string CAPTION;

        [JsonProperty(PropertyName = "ARTWORK DATA.CLASSIFICATION")]
        public string CLASSIFICATION;

        //[JsonProperty(PropertyName = "ARTWORK DATA.COPYRIGHT")]
        //public string COPYRIGHT;

        [JsonProperty(PropertyName = "ARTWORK DATA.COPYRIGHT")]
        public string[] COPYRIGHTS;

        [JsonProperty(PropertyName = "ARTWORK DATA.CREDITLINE")]
        public string CREDITLINE;

        [JsonProperty(PropertyName = "ARTWORK DATA.DATE_OF_ART_CREATED")]
        public string DATE_OF_ART_CREATED;

        [JsonProperty(PropertyName = "ARTWORK DATA.DESCRIPTION")]
        public string DESCRIPTION;

        [JsonProperty(PropertyName = "ARTWORK DATA.DIMENSIONS")]
        public string DIMENSIONS;

        [JsonProperty(PropertyName = "ARTWORK DATA.DIMENSIONS DATA")]
        public DimensionData[] DIMENSIONS_DATA;


        [JsonProperty(PropertyName = "ARTWORK DATA.DISPLAY_LOCATION")]
        public string DISPLAY_LOCATION;

        [JsonProperty(PropertyName = "ARTWORK DATA.DOMAIN")]
        public string DOMAIN;

        //[JsonProperty(PropertyName = "ARTWORK DATA.EXHIBITION_HISTORY")]
        //public string EXHIBITION_HISTORY;

        [JsonProperty(PropertyName = "ARTWORK DATA.EXHIBITION_HISTORY")]
        public EXHIBITION_HISTORY[] EXHIBITION_HISTORIES;

        [JsonProperty(PropertyName = "ARTWORK DATA.GENRE")]
        public string GENRE;


        //Product_Location
        [JsonProperty(PropertyName = "ARTWORK DATA.GEOGRAPHIC_ASSOCIATION")]
        public string GEOGRAPHIC_ASSOCIATION;

        [JsonProperty(PropertyName = "ARTWORK DATA.ID")]
        public string ID;


        [JsonProperty(PropertyName = "ARTWORK DATA.KEYWORDS")]
        public string KEYWORDS;
        [JsonProperty(PropertyName = "ARTWORK DATA.MEDIUM")]
        public string MEDIUM;

        [JsonProperty(PropertyName = "ARTWORK DATA.OBJECT_STATUS")]
        public string OBJECT_STATUS;

        [JsonProperty(PropertyName = "ARTWORK DATA.ON_DISPLAY")]
        public string ON_DISPLAY;

        [JsonProperty(PropertyName = "ARTWORK DATA.OWNER")]
        public string OWNER;

        [JsonProperty(PropertyName = "ARTWORK DATA.PHOTOGRAPHER")]
        public string PHOTOGRAPHER;

        [JsonProperty(PropertyName = "ARTWORK DATA.PHYSICAL_DISPLAY")]
        public string PHYSICAL_DISPLAY;
        [JsonProperty(PropertyName = "ARTWORK DATA.PRIMARY")]
        public string PRIMARY;
        [JsonProperty(PropertyName = "ARTWORK DATA.PUBLISHED DIMENSION")]
        public string PUBLISHED_DIMENSION;
        [JsonProperty(PropertyName = "ARTWORK DATA.PUBLISHED_ON")]
        public string PUBLISHED_ON;
        [JsonProperty(PropertyName = "ARTWORK DATA.RESTRICTEDORNONRESTRICTED")]
        public string RESTRICTEDORNONRESTRICTED;
        [JsonProperty(PropertyName = "ARTWORK DATA.RESTRICTION")]
        public string RESTRICTION;
        [JsonProperty(PropertyName = "ARTWORK DATA.SYSTEM_ID")]
        public string SYSTEM_ID;
        //[JsonProperty(PropertyName = "ARTWORK DATA.TITLE")]
        //public string TITLE;
        [JsonProperty(PropertyName = "ARTWORK DATA.TITLE")]
        public string[] TITLES;
        [JsonProperty(PropertyName = " ARTWORK DATA.RIGHTS_REMARKS")]
        public string[] RIGHTS_REMARKS;
       

        #endregion

        #region OCSP_CONTROL
        [JsonProperty(PropertyName = "OCSP_CONTROL.ARCHIVES_ACCESS_LEVEL")]      
        public string ARCHIVES_ACCESS_LEVEL;

        [JsonProperty(PropertyName = "OCSP_CONTROL.ARCHIVES_PUBLISH_TO_OCSP")]
        public string ARCHIVES_PUBLISH_TO_OCSP;

        [JsonProperty(PropertyName = "OCSP_CONTROL.ARTWORK_ACCESS_LEVEL")]
        public string ARTWORK_ACCESS_LEVEL;     

        [JsonProperty(PropertyName = "OCSP_CONTROL.ARTWORK_PUBLISH_TO_OCSP")]
        public string ARTWORK_PUBLISH_TO_OCSP;
          
        #endregion

        #region Archive
        [JsonProperty(PropertyName = "RC.REFERENCE_CODE")]
        public string REFERENCE_CODE;


        [JsonProperty(PropertyName = "RC.TITLE")]       
        public string ARC_TITLE;

        [JsonProperty(PropertyName = "RC.OTHER_TITLES")]
        public string OTHER_TITLES;      

        [JsonProperty(PropertyName = "RC.FONDS-COLLECTIONS")]
        public string FONDS_COLLECTIONS;

        [JsonProperty(PropertyName = "RC.CREATORS")]
        public string CREATORS;


        [JsonProperty(PropertyName = "RC.TYPE_OF_DATE")]
        public string TYPE_OF_DATE;

        [JsonProperty(PropertyName = "RC.DISPLAY_DATE1")]
        public string DISPLAY_DATE1;
        [JsonProperty(PropertyName = "RC.DISPLAY_DATE2")]
        public string DISPLAY_DATE2;


        [JsonProperty(PropertyName = "RC.LANGUAGE_OF_MATERIALS")]
        public string LANGUAGE;

        [JsonProperty(PropertyName = "RC.SUB_PERSONAL_NAME")]
        public string SUB_PERSONAL_NAME;
        [JsonProperty(PropertyName = "RC.SUB_PLACE_NAME")]// Location
        public string SUB_PLACE_NAME;
        [JsonProperty(PropertyName = "RC.SUB_INSTITUTION_NAME")]
        public string SUB_INSTITUTION_NAME;
        [JsonProperty(PropertyName = "RC.SUB_TOPICAL_TERM")]
        public string SUB_TOPICAL_TERM;


        [JsonProperty(PropertyName = "RC.GENERAL_MATERIAL_TYPE")]
        public string GENERAL_MATERIAL_TYPE;

        [JsonProperty(PropertyName = "RC.EXTENT_AND_MEDIUM")]
        public string EXTENT_AND_MEDIUM;

        [JsonProperty(PropertyName = "RC.STORAGE_LOCATION")]
        public string STORAGE_LOCATION;

        [JsonProperty(PropertyName = "RC.ACCESS_RESTRICTIONS_CODE")]
        public string AccessLevel;

        [JsonProperty(PropertyName = "RC.CONDITIONS_GOVERNING_ACCESS")]
        public string CONDITIONS_GOVERNING_ACCESS;

        [JsonProperty(PropertyName = "RC.USAGE_RESTRICTIONS_CODE")]
        public string USAGE_RESTRICTIONS_CODE;

        [JsonProperty(PropertyName = "RC.CONDITIONS_GOVERNING_USE_AND_REPRODUCTION")]
        public string CONDITIONS_GOVERNING_USE_AND_REPRODUCTION;

        [JsonProperty(PropertyName = "RC.SYSTEM_OF_ARRANGEMENT")]
        public string SYSTEM_OF_ARRANGEMENT;

        [JsonProperty(PropertyName = "RC.ARCHIVAL_HISTORY")]
        public string ARCHIVAL_HISTORY;
       
        [JsonProperty(PropertyName = "RC.IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER")]
        public string IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER;

        [JsonProperty(PropertyName = "RC.EXISTENCE_AND_LOCATION_OF_ORIGINALS")]
        public string EXISTENCE_AND_LOCATION_OF_ORIGINALS;

        [JsonProperty(PropertyName = "RC.EXISTENCE_AND_LOCATION_OF_COPIES")]
        public string EXISTENCE_AND_LOCATION_OF_COPIES;

        [JsonProperty(PropertyName = "RC.PUBLICATION_NOTE")]
        public string PUBLICATION_NOTE;
        [JsonProperty(PropertyName = "RC.GENERAL_NOTES")]
        public string GENERAL_NOTES;
        [JsonProperty(PropertyName = "RC.CONSERVATION_NOTES")]
        public string CONSERVATION_NOTES;


        [JsonProperty(PropertyName = "RC.SCOPE_AND_CONTENT")]
        public string SCOPE_AND_CONTENT;


        [JsonProperty(PropertyName = "RC.STATUS")]
        public string STATUS;

        [JsonProperty(PropertyName = "RC.LEVEL_OF_DETAIL")]
        public string LEVEL_OF_DETAIL;


        [JsonProperty(PropertyName = "RC.LEVEL_OF_DESCRIPTION")]
        public string LEVEL_OF_DESCRIPTION;


       
        #endregion

    }

    public class DimensionData
    {
        [JsonProperty(PropertyName = "ARTWORK DATA.DIMENSIONS DATA:DIMENSION U_SUMMARY DATA")]
        public string U_SUMMARY;

        [JsonProperty(PropertyName = "ARTWORK DATA.DIMENSIONS DATA:DIMENSION TYPE")]
        public string DIMENSIONTYPE;

    }


    public class Derivs
    {
        public string order;
        public string url;
        public string name;
    }

    public class Thumbnail
    {
        public string order;
        public string url;
        public string name;
    }


    public class ARTIST_INFO
    {
        [JsonProperty(PropertyName = "ARTWORK DATA.ARTIST:ARTIST_NAME")]
        public string ARTIST_NAME;

        [JsonProperty(PropertyName = "ARTWORK DATA.ARTIST:ARTIST_DOB")]
        public string ARTIST_DOB;

        [JsonProperty(PropertyName = "ARTWORK DATA.ARTIST:ARTIST_DOD")]
        public string ARTIST_DOD;

    }


    public class EXHIBITION_HISTORY
    {
        [JsonProperty(PropertyName = "ARTWORK DATA.EXHIBITION_HISTORY:DECISION")]
        public string DECISION;

        [JsonProperty(PropertyName = "ARTWORK DATA.EXHIBITION_HISTORY:DISPLAY_LOCATION")]
        public string DISPLAY_LOCATION;

        [JsonProperty(PropertyName = "ARTWORK DATA.EXHIBITION_HISTORY:EXHIBITION_TITLE")]
        public string EXHIBITION_TITLE;

    }
}
