﻿using ImportData.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ImportData
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //One Time Load
            //if (args != null && args.Count()>0)
            //{
            //    string para = args[0].Trim().ToUpper();
            //    

            //    switch (para)
            //    {
            //        case "LIB":
            //            LibraryService.ImportLibraryData();
            //            LibraryService.ImportLibraryItemData();
            //            break;
            //        case "ART_ARC":
            //            LogService.CreateErrorLog("Start Artwork");
            //            ArtworkService.ImportArtworkData();
            //            LogService.CreateErrorLog("Start Archive");
            //            ArchiveService.ImportArchiveData();
            //            break;
            //        default:
            //            LogService.CreateFailImportLog("Failed import : Parameters(LIB or ART_ARC are missing");
            //            break;

            //    }
            //}
            //Everyday Sync 
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            if (args != null && args.Count() > 0)
            {
                string option = "";
                string fromDate = "";
                string toDate = "";
                string irn = "";
                string para = args[0].Trim().ToUpper();
                if(args.Count() > 1)
                {
                    if (!string.IsNullOrEmpty(args[1]))
                    {
                        option = args[1].Trim().ToUpper();
                        if (option == "1")
                        {
                            if(args.Count() > 3)
                            {
                                fromDate = args[2].Trim().ToUpper();
                                toDate = args[3].Trim().ToUpper();
                            }
                        }
                        else if (option == "2")
                        {
                            if (args.Count() > 2)
                            {
                                irn = args[2].Trim().ToUpper();
                            }
                        }
                    }
                }
                switch (para)
                {
                    case "LIB":
                        //LibraryService.ImportLibraryDataUpdate();
                        //"LIB" "1" "2021-03-12" "2021-03-14" "345"
                        //1: fromdate, todate
                        //2: irn
                        await LibraryService.ImportLibraryDataFromCivicaApi(option, fromDate, toDate, irn);
                        break;
                    case "ITEM":    // 15 mins                    
                        //LibraryService.ImportLibraryItemDataUpdate();
                        //"ITEM" "1" "2021-03-12" "2021-03-14" "345"
                        //1: fromdate, todate
                        //2: irn
                        await LibraryService.ImportLibraryItemDataFromCivicaApi(option, fromDate, toDate, irn);
                        break;
                    case "ART_ARC":
                        ArtworkService.ImportArtworkData_ToUpdate();
                        ArchiveService.ImportArchiveData_ToUpdate();
                        break;
                    case "DELETE":
                        UMOService.DeleteByUMOID();
                        break;
                    case "ART_ARC_BACKDATE": // check one time (7 days)
                        //ArtworkService.ImportArtworkData_ToUpdateByBackDate();
                        //ArchiveService.ImportArchiveData_ToUpdateByBackDate();
                        break;
                    default:
                        LogService.CreateFailImportLog("Failed import : Parameters(LIB or ART_ARC or ITEM or DELETE or ART_ARC_BACKDATE are missing");
                        break;

                }
            }

            ////To sync by date between (customize date) 
            //if (args != null && args.Count() > 2)
            //{
            //    string para = args[0].Trim().ToUpper();
            //    string startDate = args[1].Trim().ToUpper();
            //    string endDate = args[2].Trim().ToUpper();

            //    switch (para)
            //    {                    
            //        case "ART":
            //            ArtworkService.ImportArtworkData_ToUpdateByCheckDate(startDate,endDate);                     
            //            break;
            //        case "ARC":                     
            //            ArchiveService.ImportArchiveData_ToUpdateByCheckDate(startDate,endDate);
            //            break;
            //        case "DELETE":
            //            UMOService.DeleteByUMOID(startDate);
            //            break;                  
            //        default:
            //            LogService.CreateFailImportLog("Failed import : Parameters(LIB or ART or ARC or DELETE are missing");
            //            break;

            //    }
            //}


        }

    }
}
