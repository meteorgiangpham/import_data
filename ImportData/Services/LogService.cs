﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportData.Services
{
    public class LogService
    {

        public LogService()
        {

        }

        public static string logPath = ConfigurationManager.AppSettings["Log.Path"];
        private static void CreateLog(string fileName, string message)
        {
            //var path = Config.Get("log.path", true) + "\\";
            var path = logPath + "\\";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string location = path + fileName;

            if (!File.Exists(location))
            {
                FileStream fs;
                using (fs = new FileStream(location, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                {

                }

                fs.Close();
            }

            //Release the File that is created
            StreamWriter sw = new StreamWriter(location, true);
            sw.Write(message + Environment.NewLine);
            sw.Close();
            sw = null;

        }
        
        public static void CreateDeleteLog(String message)
        {
            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMdd") + ".deletedata.log" + ".txt";
                string errorLog = DateTime.Now.ToString() + " |   " + message;
                CreateLog(fileName, errorLog);

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Rotunda", "Error in Create LogFile" + ex.Message.ToString(), EventLogEntryType.Error, 6000);
            }
        }

        public static void CreateErrorLog(String message)
        {
            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMdd") + ".importerrors.log" + ".txt";
                string errorLog = DateTime.Now.ToString() + " |   " +  message;
                CreateLog(fileName, errorLog);

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Rotunda", "Error in Create Custom LogFile" + ex.Message.ToString(), EventLogEntryType.Error, 6000);
            }
        }

        public static void CreateS3ErrorLog(String message)
        {
            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMdd") + ".s3errors.log" + ".txt";
                string errorLog = DateTime.Now.ToString() + " |   " + message;
                CreateLog(fileName, errorLog);

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Rotunda", "Error in Create Custom LogFile" + ex.Message.ToString(), EventLogEntryType.Error, 6000);
            }
        }


        public static void CreateFailImportLog(String message)
        {
            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMdd") + ".importfail.log" + ".txt";
                string errorLog = DateTime.Now.ToString() + " |   " + message;
                CreateLog(fileName, errorLog);

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Rotunda", "Error in Create Custom LogFile" + ex.Message.ToString(), EventLogEntryType.Error, 6000);
            }
        }


        public static void CreateImportActivityLog(String message)
        {
            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMdd") + ".import.log" + ".txt";
                string errorLog = DateTime.Now.ToString() + " |   " + message;
                CreateLog(fileName, errorLog);

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Rotunda", "Error in Create Custom LogFile" + ex.Message.ToString(), EventLogEntryType.Error, 6000);
            }
        }
    }
}
