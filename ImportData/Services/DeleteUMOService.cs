﻿using ImportData.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ImportData
{
    public class UMOService
    {
        public static string deleteApiUrl = ConfigurationManager.AppSettings["Api.DeleteData.Url"];

        //https://dmz.nationalgallery.sg/r/st/deleted_umo/surl/{0}/ACTION/UMO/DATE_AFTER/{1}/JSON/T
        //delete_url_org = "https://dmz.nationalgallery.sg/r/st/deleted_umo/surl/800218629ZZZOVUDJBPTIX/ACTION/UMO/DATE_AFTER/29-JUL-2019/JSON/T";

        public static void DeleteByUMOID()
        {
            try
            {
                var auth = AuthService.GetAuth();              

                if (auth != null)
                {

                    string checkDate = DateTime.Now.AddDays(-1).ToString("dd-MM-yyyy");                    

                    var authDeleteURL = String.Format(deleteApiUrl, auth.SURL, checkDate);
                  

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(authDeleteURL);//
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";


                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");//***
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }


                    dynamic data = JsonConvert.DeserializeObject(myResponse);

                    var deserialized = JsonConvert.DeserializeObject<DeletedUMOs>(myResponse);
                 

                    foreach (UMOs r in deserialized.deleted)
                    {
                        if(r!=null && r.umo!=null)
                        {
                            LogService.CreateDeleteLog("UMO ID  : " + r.umo + " ,UMO's dc   : " + r.dc);

                            DBService.ProductUpdateActiveByUMOID(r.umo, false);
                        }        


                    }



                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }


        public static void DeleteByUMOID(string checkDate)
        {
            try
            {
                var auth = AuthService.GetAuth();              

                if (auth != null)
                {

                    string deleteDate = Convert.ToDateTime(checkDate).ToString("dd-MM-yyyy");

                    var authDeleteURL = String.Format(deleteApiUrl, auth.SURL, deleteDate);


                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(authDeleteURL);//
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";


                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");//***
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }


                    dynamic data = JsonConvert.DeserializeObject(myResponse);

                    var deserialized = JsonConvert.DeserializeObject<DeletedUMOs>(myResponse);


                    foreach (UMOs r in deserialized.deleted)
                    {
                        if (r != null && r.umo != null)
                        {
                            LogService.CreateDeleteLog("UMO ID  : " + r.umo + " ,UMO's dc   : " + r.dc);

                            DBService.ProductUpdateActiveByUMOID(r.umo, false);
                        }


                    }



                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }

    }
}
