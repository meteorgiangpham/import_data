﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using ImportData.Services;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.S3.Model;

namespace ImportData
{
    public class ArtworkService
    {

        public static string artwork_url = ConfigurationManager.AppSettings["ART.Api.Url"];
        public static string urlForUpdatedData = ConfigurationManager.AppSettings["ART.Api.UpdateData.Url"];
        static string destinationpath = ConfigurationManager.AppSettings["ART.Download.Path"];

        private static void SaveArtworks(List<ART_ARC> data)
        {
            try
            {
                string imagePath = string.Empty;
                string filePath = string.Empty;
                string dimensions = string.Empty;
                int? systemId;

                int succeed = 0;
                int failed = 0;

                string titles = string.Empty;
                string artists = string.Empty;
                string dobs = string.Empty;
                string dods = string.Empty;
                string copyRights = string.Empty;
                string remarks = string.Empty;

                //***
                data = data.Where(x => x.metadata != null && x.metadata.ARTWORK_PUBLISH_TO_OCSP.ToUpper() == "YES").ToList();

                #region Save/ Update Data
                foreach (ART_ARC r in data.Where(x => x.metadata != null))
                {
                    if (!String.IsNullOrEmpty(r.metadata.SYSTEM_ID) && r.metadata.TITLES != null)
                    {
                        systemId = Convert.ToInt32(r.metadata.SYSTEM_ID);

                    }

                    else
                    {
                        systemId = (int?)null;
                        LogService.CreateErrorLog("Artwork : No SystemId, Title, Artist For Record UMO ID :" + r.umo_id);
                        failed += 1;
                        continue;

                    }

                    if (r.metadata.TITLES != null && r.metadata.TITLES.Count() > 0)
                    {
                        titles = String.Join("|", r.metadata.TITLES);
                    }
                    else
                    {
                        LogService.CreateErrorLog("Artwork : No Title For Record UMO ID :" + r.umo_id);
                        failed += 1;
                        continue;
                    }

                    if (r.derivs.Count() > 3)
                    {
                        imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
                        filePath = DownloadFile(r.derivs[3].url, r.umo_id);
                    }
                    else if (r.derivs.Count() > 0)
                    {
                        imagePath = string.Empty;
                        filePath = DownloadFile(r.derivs[0].url, r.umo_id);
                    }
                    else
                    {
                        LogService.CreateErrorLog("Artwork : No derivs For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID);
                        imagePath = null;
                        filePath = null;
                    }

                    if (String.IsNullOrEmpty(r.metadata.ARTWORK_ACCESS_LEVEL))
                    {
                        LogService.CreateErrorLog("Artwork : Missing ARTWORK_ACCESS_LEVEL For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID);
                    }

                    dimensions = string.Empty;
                    if (r.metadata.DIMENSIONS_DATA != null && r.metadata.DIMENSIONS_DATA.Count() > 0)
                    {
                        foreach (DimensionData d in r.metadata.DIMENSIONS_DATA)
                        {
                            dimensions += !String.IsNullOrEmpty(d.DIMENSIONTYPE) ? (!String.IsNullOrEmpty(dimensions) ? "|" : "") + d.DIMENSIONTYPE + " : " + d.U_SUMMARY : ""; //***

                        }
                    }


                    #region Artist
                    if (r.metadata.ARTISTS != null && r.metadata.ARTISTS.Count() > 0)
                    {
                        artists = string.Empty;
                        dobs = string.Empty;
                        dods = string.Empty;

                        foreach (ARTIST_INFO d in r.metadata.ARTISTS)
                        {
                            if (d.ARTIST_NAME != null)
                            {

                                artists += String.IsNullOrEmpty(d.ARTIST_NAME) ? "|" : d.ARTIST_NAME + (d == r.metadata.ARTISTS.Last() ? "" : "|");
                                dobs += String.IsNullOrEmpty(d.ARTIST_DOB) ? "|" : d.ARTIST_DOB + (d == r.metadata.ARTISTS.Last() ? "" : "|");
                                dods += String.IsNullOrEmpty(d.ARTIST_DOD) ? "|" : d.ARTIST_DOD + (d == r.metadata.ARTISTS.Last() ? "" : "|");
                            }

                        }
                    }
                    #endregion


                    if (r.metadata.COPYRIGHTS != null && r.metadata.COPYRIGHTS.Count() > 0)
                    {
                        copyRights = string.Empty;
                        copyRights = String.Join("|", r.metadata.COPYRIGHTS);
                    }

                    if (r.metadata.RIGHTS_REMARKS != null && r.metadata.RIGHTS_REMARKS.Count() > 0)
                    {
                        remarks = string.Empty;
                        remarks = String.Join("|", r.metadata.RIGHTS_REMARKS);
                    }

                    int result = DBService.ArtworkInsert(r.umo_id, filePath, imagePath, titles, artists, r.metadata.DATE_OF_ART_CREATED, r.metadata.GEOGRAPHIC_ASSOCIATION, r.metadata.ARTWORK_ACCESS_LEVEL
                        , systemId, r.metadata.ACCESSIONNUMBER, dobs, dods, r.metadata.CREDITLINE, r.metadata.MEDIUM
                        , dimensions, r.metadata.ON_DISPLAY, r.metadata.PHOTOGRAPHER, r.metadata.GENRE, copyRights, null, r.metadata.DISPLAY_LOCATION, remarks, r.metadata.DESCRIPTION);


                    #region EXHIBITION_HISTORIES
                    if (r.metadata.EXHIBITION_HISTORIES != null && r.metadata.EXHIBITION_HISTORIES.Count() > 0)
                    {
                        foreach (EXHIBITION_HISTORY d in r.metadata.EXHIBITION_HISTORIES)
                        {
                            DBService.ExhibitionHistoryInsert(r.umo_id, d.DECISION, d.DISPLAY_LOCATION, d.EXHIBITION_TITLE);

                        }
                    }
                    #endregion


                    if (result > 0) succeed += 1;
                    else failed += 1;


                }
                #endregion

                #region Sync Log
                DBService.SyncLogsInsert("ART", data.Where(x => x.metadata != null).Count(), succeed, failed);
                #endregion

            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }

        public static void ImportArtworkDataTEST()
        {
            try
            {


                string artfilePath = Path.Combine("D:\\NGS\\ImportData_Artwork\\ImportData", "ART.json");

                LogService.CreateImportActivityLog("Import item file  : " + artfilePath + " |    Current DateTime : " + DateTime.Now.ToString());


                JArray o1 = JArray.Parse(File.ReadAllText(artfilePath));


                var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(o1.ToString());

                string imagePath = string.Empty;
                string filePath = string.Empty;
                string dimensions = string.Empty;
                int? systemId;

                int succeed = 0;
                int failed = 0;
                string titles = string.Empty;
                string artists = string.Empty;
                string dobs = string.Empty;
                string dods = string.Empty;
                string copyRights = string.Empty;
                string remarks = string.Empty;

                foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
                {
                    if (!String.IsNullOrEmpty(r.metadata.SYSTEM_ID) && r.metadata.TITLES != null)
                    {
                        systemId = Convert.ToInt32(r.metadata.SYSTEM_ID);

                    }

                    else
                    {
                        systemId = (int?)null;
                        LogService.CreateErrorLog("Artwork : No SystemId, Title, Artist For Record UMO ID :" + r.umo_id);
                        failed += 1;
                        continue;

                    }

                    if (r.metadata.TITLES != null && r.metadata.TITLES.Count() > 0)
                    {
                        titles = String.Join("|", r.metadata.TITLES);
                    }
                    else
                    {
                        LogService.CreateErrorLog("Artwork : No Title For Record UMO ID :" + r.umo_id);
                        failed += 1;
                        continue;
                    }

                    if (r.derivs.Count() > 3)
                    {
                        //imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
                        //filePath = DownloadFile(r.derivs[3].url, r.umo_id);
                    }
                    else if (r.derivs.Count() > 0)
                    {
                        //imagePath = string.Empty;
                        //filePath = DownloadFile(r.derivs[0].url, r.umo_id);
                    }
                    else
                    {
                        LogService.CreateErrorLog("Artwork : No derivs For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID);
                        imagePath = null;
                        filePath = null;
                    }

                    if (String.IsNullOrEmpty(r.metadata.ARTWORK_ACCESS_LEVEL))
                    {
                        LogService.CreateErrorLog("Artwork : Missing ARTWORK_ACCESS_LEVEL For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID);
                    }

                    dimensions = string.Empty;
                    if (r.metadata.DIMENSIONS_DATA != null && r.metadata.DIMENSIONS_DATA.Count() > 0)
                    {
                        foreach (DimensionData d in r.metadata.DIMENSIONS_DATA)
                        {
                            dimensions += !String.IsNullOrEmpty(d.DIMENSIONTYPE) ? (!String.IsNullOrEmpty(dimensions) ? "|" : "") + d.DIMENSIONTYPE + " : " + d.U_SUMMARY : ""; //***

                        }
                    }


                    #region Artist
                    if (r.metadata.ARTISTS != null && r.metadata.ARTISTS.Count() > 0)
                    {
                        artists = string.Empty;
                        dobs = string.Empty;
                        dods = string.Empty;

                        foreach (ARTIST_INFO d in r.metadata.ARTISTS)
                        {
                            if(r.umo_id== "11915863")
                            {
                                //tocheck;
                                var aa = d;
                            }
                            if (d.ARTIST_NAME != null)
                            {
                                artists += String.IsNullOrEmpty(d.ARTIST_NAME) ? "|" : d.ARTIST_NAME + (d == r.metadata.ARTISTS.Last() ? "" : "|");
                                dobs += String.IsNullOrEmpty(d.ARTIST_DOB) ? "|" : d.ARTIST_DOB + (d == r.metadata.ARTISTS.Last() ? "" : "|");
                                dods += String.IsNullOrEmpty(d.ARTIST_DOD) ? "|" : d.ARTIST_DOD + (d == r.metadata.ARTISTS.Last() ? "" : "|");

                            }
                        }
                    }
                    #endregion


                    if (r.metadata.COPYRIGHTS != null && r.metadata.COPYRIGHTS.Count() > 0)
                    {
                        copyRights = string.Empty;
                        copyRights = String.Join("|", r.metadata.COPYRIGHTS);
                    }

                    if (r.metadata.RIGHTS_REMARKS != null && r.metadata.RIGHTS_REMARKS.Count() > 0)
                    {
                        remarks = string.Empty;
                        remarks = String.Join("|", r.metadata.RIGHTS_REMARKS);
                    }

                    //////int result = DBService.ArtworkInsert(r.umo_id, filePath, imagePath, titles, artists, r.metadata.DATE_OF_ART_CREATED, r.metadata.GEOGRAPHIC_ASSOCIATION, r.metadata.ARTWORK_ACCESS_LEVEL
                    //////    , systemId, r.metadata.ACCESSIONNUMBER, dobs, dods, r.metadata.CREDITLINE, r.metadata.MEDIUM
                    //////    , dimensions, r.metadata.ON_DISPLAY, r.metadata.PHOTOGRAPHER, r.metadata.GENRE, copyRights, null, r.metadata.DISPLAY_LOCATION, remarks);


                    #region EXHIBITION_HISTORIES
                    if (r.metadata.EXHIBITION_HISTORIES != null && r.metadata.EXHIBITION_HISTORIES.Count() > 0)
                    {
                        foreach (EXHIBITION_HISTORY d in r.metadata.EXHIBITION_HISTORIES)
                        {
                            DBService.ExhibitionHistoryInsert(r.umo_id, d.DECISION, d.DISPLAY_LOCATION, d.EXHIBITION_TITLE);

                        }
                    }
                    #endregion


                    ////if (result > 0) succeed += 1;
                    ////else failed += 1;


                }

                #region Sync Log
                //DBService.SyncLogsInsert("ART", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
                #endregion

                //AUTH auth = new AUTH();
                //auth.SURL = "1863052407ZZZHAABKSDAJ";


                LogService.CreateImportActivityLog("Import all artwork data : current date time :" + DateTime.Now.ToString());

            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }

        public static void ImportArtworkData()
        {
            try
            {
                var auth = AuthService.GetAuth();
                //AUTH auth = new AUTH();
                //auth.SURL = "1863052407ZZZHAABKSDAJ";


                LogService.CreateImportActivityLog("Import all artwork data : current date time :" + DateTime.Now.ToString());

                if (auth != null)
                {
                    var artworkURL = String.Format(artwork_url, auth.SURL);

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
                    int test = request.Timeout;
                    request.Timeout = 3600000;
                    //request.Timeout = 200;
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";


                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");//***
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }


                    dynamic data = JsonConvert.DeserializeObject(myResponse);

                    var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);

                    SaveArtworks(deserialized);



                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }

        /// <summary>
        /// every day sync job (to get update/new data)
        /// </summary>
        public static void ImportArtworkData_ToUpdate()
        {
            try
            {
                var auth = AuthService.GetAuth();

                LogService.CreateImportActivityLog("auth data: " + JsonConvert.SerializeObject(auth));

                if (auth != null)
                {
                    string fromDate = DateTime.Now.AddDays(-15).ToString("dd-MM-yyyy");

                    string toDate = DateTime.Now.ToString("dd-MM-yyyy");


                    LogService.CreateImportActivityLog("Import daily artwork data  between from date : ( " + fromDate + " )  and to date : ( " + toDate + " )  , current date time :" + DateTime.Now.ToString());


                    var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
                    int test = request.Timeout;
                    request.Timeout = 3600000;
                    //request.Timeout = 200;
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";


                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");//***
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }


                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

                    //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
                    dynamic data = JsonConvert.DeserializeObject(myResponse);

                    var hasData = data.Count;
                    if (hasData <= 1)
                    {
                        #region Sync Log
                        DBService.SyncLogsInsert("ART", 0, 0, 0);
                        #endregion

                        return;
                    }

                    LogService.CreateImportActivityLog("ART data: " + myResponse);
                    var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);

                    SaveArtworks(deserialized);

                    #region Old

                    //////string imagePath = string.Empty;
                    //////string filePath = string.Empty;
                    //////string dimensions = string.Empty;
                    //////int? systemId;

                    //////int succeed = 0;
                    //////int failed = 0;

                    //////string titles = string.Empty;
                    //////string artists = string.Empty;
                    //////string dobs = string.Empty;
                    //////string dods = string.Empty;
                    //////string copyRights = string.Empty;
                    //////string remarks = string.Empty;

                    //////#region Save/ Update Data
                    //////foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
                    //////{
                    //////    if (!String.IsNullOrEmpty(r.metadata.SYSTEM_ID) && r.metadata.TITLES != null)
                    //////    {
                    //////        systemId = Convert.ToInt32(r.metadata.SYSTEM_ID);

                    //////    }

                    //////    else
                    //////    {
                    //////        systemId = (int?)null;
                    //////        LogService.CreateErrorLog("Artwork : No SystemId, Title, Artist For Record UMO ID :" + r.umo_id);
                    //////        failed += 1;
                    //////        continue;

                    //////    }

                    //////    if (r.metadata.TITLES != null && r.metadata.TITLES.Count() > 0)
                    //////    {
                    //////        titles = String.Join("|", r.metadata.TITLES);
                    //////    }
                    //////    else
                    //////    {
                    //////        LogService.CreateErrorLog("Artwork : No Title For Record UMO ID :" + r.umo_id);
                    //////        failed += 1;
                    //////        continue;
                    //////    }

                    //////    if (r.derivs.Count() > 3)
                    //////    {
                    //////        imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
                    //////        filePath = DownloadFile(r.derivs[3].url, r.umo_id);
                    //////    }
                    //////    else if (r.derivs.Count() > 0)
                    //////    {
                    //////        imagePath = string.Empty;
                    //////        filePath = DownloadFile(r.derivs[0].url, r.umo_id);
                    //////    }
                    //////    else
                    //////    {
                    //////        LogService.CreateErrorLog("Artwork : No derivs For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID);
                    //////        imagePath = null;
                    //////        filePath = null;
                    //////    }

                    //////    if (String.IsNullOrEmpty(r.metadata.ARTWORK_ACCESS_LEVEL))
                    //////    {
                    //////        LogService.CreateErrorLog("Artwork : Missing ARTWORK_ACCESS_LEVEL For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID);
                    //////    }

                    //////    dimensions = string.Empty;
                    //////    if (r.metadata.DIMENSIONS_DATA != null && r.metadata.DIMENSIONS_DATA.Count() > 0)
                    //////    {
                    //////        foreach (DimensionData d in r.metadata.DIMENSIONS_DATA)
                    //////        {
                    //////            dimensions += !String.IsNullOrEmpty(d.DIMENSIONTYPE) ? (!String.IsNullOrEmpty(dimensions) ? "|" : "") + d.DIMENSIONTYPE + " : " + d.U_SUMMARY : ""; //***

                    //////        }
                    //////    }


                    //////    #region Artist
                    //////    if (r.metadata.ARTISTS != null && r.metadata.ARTISTS.Count() > 0)
                    //////    {
                    //////        artists = string.Empty;
                    //////        dobs = string.Empty;
                    //////        dods = string.Empty;

                    //////        foreach (ARTIST_INFO d in r.metadata.ARTISTS)
                    //////        {
                    //////            artists += String.IsNullOrEmpty(d.ARTIST_NAME) ? "|" : d.ARTIST_NAME + (d == r.metadata.ARTISTS.Last() ? "" : "|");
                    //////            dobs += String.IsNullOrEmpty(d.ARTIST_DOB) ? "|" : d.ARTIST_DOB + (d == r.metadata.ARTISTS.Last() ? "" : "|");
                    //////            dods += String.IsNullOrEmpty(d.ARTIST_DOD) ? "|" : d.ARTIST_DOD + (d == r.metadata.ARTISTS.Last() ? "" : "|");

                    //////        }
                    //////    }
                    //////    #endregion


                    //////    if (r.metadata.COPYRIGHTS != null && r.metadata.COPYRIGHTS.Count() > 0)
                    //////    {
                    //////        copyRights = string.Empty;
                    //////        copyRights = String.Join("|", r.metadata.COPYRIGHTS);
                    //////    }

                    //////    if (r.metadata.RIGHTS_REMARKS != null && r.metadata.RIGHTS_REMARKS.Count() > 0)
                    //////    {
                    //////        remarks = string.Empty;
                    //////        remarks = String.Join("|", r.metadata.RIGHTS_REMARKS);
                    //////    }

                    //////    int result = DBService.ArtworkInsert(r.umo_id, filePath, imagePath, titles, artists, r.metadata.DATE_OF_ART_CREATED, r.metadata.GEOGRAPHIC_ASSOCIATION, r.metadata.ARTWORK_ACCESS_LEVEL
                    //////        , systemId, r.metadata.ACCESSIONNUMBER, dobs, dods, r.metadata.CREDITLINE, r.metadata.MEDIUM
                    //////        , dimensions, r.metadata.ON_DISPLAY, r.metadata.PHOTOGRAPHER, r.metadata.GENRE, copyRights, null, r.metadata.DISPLAY_LOCATION, remarks);


                    //////    #region EXHIBITION_HISTORIES
                    //////    if (r.metadata.EXHIBITION_HISTORIES != null && r.metadata.EXHIBITION_HISTORIES.Count() > 0)
                    //////    {
                    //////        foreach (EXHIBITION_HISTORY d in r.metadata.EXHIBITION_HISTORIES)
                    //////        {
                    //////            DBService.ExhibitionHistoryInsert(r.umo_id, d.DECISION, d.DISPLAY_LOCATION, d.EXHIBITION_TITLE);

                    //////        }
                    //////    }
                    //////    #endregion


                    //////    if (result > 0) succeed += 1;
                    //////    else failed += 1;


                    //////}
                    //////#endregion

                    //////#region Sync Log
                    //////DBService.SyncLogsInsert("ART", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
                    //////#endregion

                    #endregion
                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }


        public static void ImportArtworkData_ToUpdate_SpecificDate()
        {
            try
            {
                var auth = AuthService.GetAuth();


                if (auth != null)
                {
                    string fromDate = "01-06-2020";

                    string toDate = DateTime.Now.ToString("dd-MM-yyyy");


                    LogService.CreateImportActivityLog("Import daily artwork data  between from date : ( " + fromDate + " )  and to date : ( " + toDate + " )  , current date time :" + DateTime.Now.ToString());


                    var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
                    int test = request.Timeout;
                    request.Timeout = 3600000;
                    //request.Timeout = 200;
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";


                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");//***
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }


                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

                    //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
                    dynamic data = JsonConvert.DeserializeObject(myResponse);

                    var hasData = data.Count;
                    if (hasData <= 1)
                    {
                        #region Sync Log
                        DBService.SyncLogsInsert("ART", 0, 0, 0);
                        #endregion

                        return;
                    }


                    var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);

                    SaveArtworks(deserialized);

                    
                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }

        /// <summary>
        /// 7 days check back to make sure
        /// </summary>
        //public static void ImportArtworkData_ToUpdateByBackDate()
        //{
        //    try
        //    {
        //        var auth = AuthService.GetAuth();


        //        if (auth != null)
        //        {
        //            string fromDate = DateTime.Now.AddDays(-7).ToString("dd-MM-yyyy");

        //            string toDate = DateTime.Now.ToString("dd-MM-yyyy");


        //            LogService.CreateImportActivityLog("Import weely back date check artwork data  between from date : ( " + fromDate + " )  and to date : ( " + toDate + " )  , current date time :" + DateTime.Now.ToString());


        //            var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

        //            ServicePointManager.Expect100Continue = true;
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
        //            request.Method = "Get";
        //            request.KeepAlive = true;
        //            request.ContentType = "appication/json";


        //            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");//***
        //            string myResponse = "";
        //            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
        //            {
        //                myResponse = sr.ReadToEnd();
        //            }


        //            // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

        //            //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
        //            dynamic data = JsonConvert.DeserializeObject(myResponse);

        //            var hasData = data.Count;
        //            if (hasData <= 1)
        //            {
        //                #region Sync Log
        //                DBService.SyncLogsInsert("ART", 0, 0, 0);
        //                #endregion

        //                return;
        //            }


        //            var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);


        //            string imagePath = string.Empty;
        //            string filePath = string.Empty;
        //            string dimensions = string.Empty;
        //            int? systemId;

        //            int succeed = 0;
        //            int failed = 0;


        //            foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
        //            {
        //                if (!String.IsNullOrEmpty(r.metadata.SYSTEM_ID) && !String.IsNullOrEmpty(r.metadata.TITLE))
        //                {
        //                    systemId = Convert.ToInt32(r.metadata.SYSTEM_ID);
        //                }

        //                else
        //                {
        //                    systemId = (int?)null;
        //                    LogService.CreateErrorLog("Artwork : No SystemId, Title, Artist For Record UMO ID :" + r.umo_id);
        //                    failed += 1;
        //                    continue;

        //                }

        //                if (r.derivs.Count() > 3)
        //                {
        //                    imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
        //                    filePath = DownloadFile(r.derivs[3].url, r.umo_id);
        //                }
        //                else if (r.derivs.Count() > 0)
        //                {
        //                    imagePath = null;
        //                    filePath = DownloadFile(r.derivs[0].url, r.umo_id);
        //                }
        //                else
        //                {
        //                    LogService.CreateErrorLog("Artwork : No derivs For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID + ", Title :" + r.metadata.TITLE);
        //                    imagePath = null;
        //                    filePath = null;
        //                }

        //                if (String.IsNullOrEmpty(r.metadata.ARTWORK_ACCESS_LEVEL))
        //                {
        //                    LogService.CreateErrorLog("Artwork : Missing ARTWORK_ACCESS_LEVEL For Record UMO ID :" + r.umo_id + " ,System ID " + r.metadata.SYSTEM_ID + ", Title :" + r.metadata.TITLE);
        //                }

        //                dimensions = string.Empty;
        //                if (r.metadata.DIMENSIONS_DATA != null && r.metadata.DIMENSIONS_DATA.Count() > 0)
        //                {
        //                    foreach (DimensionData d in r.metadata.DIMENSIONS_DATA)
        //                    {
        //                        dimensions += !String.IsNullOrEmpty(d.DIMENSIONTYPE) ? (!String.IsNullOrEmpty(dimensions) ? "|" : "") + d.DIMENSIONTYPE + " : " + d.U_SUMMARY : ""; //***

        //                    }
        //                }

        //                int result = DBService.ArtworkInsert(r.umo_id, filePath, imagePath, r.metadata.TITLE, r.metadata.ARTIST, r.metadata.DATE_OF_ART_CREATED, r.metadata.GEOGRAPHIC_ASSOCIATION, r.metadata.ARTWORK_ACCESS_LEVEL
        //                    , systemId, r.metadata.ACCESSIONNUMBER, r.metadata.ARTIST_BIRTHDATE, r.metadata.ARTIST_DEATH_DATE, r.metadata.CREDITLINE, r.metadata.MEDIUM
        //                    , dimensions, r.metadata.ON_DISPLAY, r.metadata.PHOTOGRAPHER, r.metadata.GENRE, r.metadata.COPYRIGHT, r.metadata.EXHIBITION_HISTORY, r.metadata.DISPLAY_LOCATION);


        //                if (result > 0) succeed += 1;
        //                else failed += 1;


        //            }

        //            #region Sync Log
        //            DBService.SyncLogsInsert("ART", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
        //            #endregion

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LogService.CreateErrorLog(e.ToString());
        //    }

        //}

        private static string DownloadFile(string url, string umoId)
        {
            try
            {

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                string filename = "";

                //string destinationpath = "C:\\Uploads\\NGS";
                if (!Directory.Exists(destinationpath))
                {
                    Directory.CreateDirectory(destinationpath);
                }


                using (HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result)
                {
                    string path = response.Headers["Content-Disposition"];
                    if (string.IsNullOrWhiteSpace(path))
                    {
                        var uri = new Uri(url);
                        filename = Path.GetFileName(uri.LocalPath);
                    }
                    else
                    {
                        ContentDisposition contentDisposition = new ContentDisposition(path);
                        filename = contentDisposition.FileName;

                    }

                    //**
                    var filepath = Path.Combine(destinationpath, filename);
                    //if (File.Exists(filepath))
                    //{
                    //    //***
                    //    //UploadToS3.UploadFileART(filepath, filename);
                    //    return filename;
                    //}
                    //**



                    var responseStream = response.GetResponseStream();
                    using (var fileStream = File.Create(System.IO.Path.Combine(destinationpath, filename)))
                    {
                        responseStream.CopyTo(fileStream);
                    }


                    long length = new System.IO.FileInfo(filepath).Length;
                    if (length < 1)
                    {
                        LogService.CreateErrorLog("Artwork : No Images For Record UMO ID :" + umoId);
                        //Delete from EC2
                        if (File.Exists(filepath))
                        {
                            File.Delete(filepath);
                        }
                        return null;
                    }

                    UploadToS3.UploadFileART(filepath, filename);

                    //Delete from EC2
                    if (File.Exists(filepath))
                    {
                        File.Delete(filepath);
                    }

                }

                return filename;
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("File Downaload Error : Detail : " + e.ToString());
                return null;
            }



        }



    }
}
