﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using ImportData.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportData
{
    public class UploadToS3
    {

        //private static string bucketName = "ngssource/stg-source/asset/Archives";// "your-amazon-s3-bucket";
        //private static string keyName = "RC-S1-CCS4.1-12_o3.jpg";
        //private static string filePath = "D:\\NGS\\Uploads\\ARC\\RC-S1-CCS4.1-12_o3.jpg";

        public static void UploadFileART(string filePath,string fileName)
        {
            string _awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            string _awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            string _bucketName = ConfigurationManager.AppSettings["ART.BucketName"];            

            var client = new AmazonS3Client(_awsAccessKey, _awsSecretKey,Amazon.RegionEndpoint.APSoutheast1);

            try
            {
                PutObjectRequest putRequest = new PutObjectRequest
                {
                    CannedACL = S3CannedACL.PublicRead,
                    BucketName = _bucketName,
                    Key = fileName,
                    FilePath = filePath,
                    ContentType = "application/octet-stream"
                };

                PutObjectResponse response = client.PutObject(putRequest);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    //throw new Exception("Check the provided AWS Credentials.");
                    LogService.CreateS3ErrorLog("Check the provided AWS Credentials.");
                }
                else
                {
                    //throw new Exception("Error occurred: " + amazonS3Exception.Message);
                    LogService.CreateS3ErrorLog("Error occurred: " + amazonS3Exception.Message);
                    return;
                }
            }
        }


        public static void UploadFileARC(string filePath, string fileName)
        {
            string _awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            string _awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            string _bucketName = ConfigurationManager.AppSettings["ARC.BucketName"];

            var client = new AmazonS3Client(_awsAccessKey, _awsSecretKey, Amazon.RegionEndpoint.APSoutheast1);

            try
            {
                PutObjectRequest putRequest = new PutObjectRequest
                {
                    CannedACL = S3CannedACL.PublicRead,
                    BucketName = _bucketName,
                    Key = fileName,
                    FilePath = filePath,
                    ContentType = "application/octet-stream"
                };

                PutObjectResponse response = client.PutObject(putRequest);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                     (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                     ||
                     amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    //throw new Exception("Check the provided AWS Credentials.");
                    LogService.CreateS3ErrorLog("Check the provided AWS Credentials.");
                }
                else
                {
                    //throw new Exception("Error occurred: " + amazonS3Exception.Message);
                    LogService.CreateS3ErrorLog("Error occurred: " + amazonS3Exception.Message);
                    return;
                }
            }
        }

        public static void UploadFileLibrary(string filePath, string fileName)
        {
            string _awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            string _awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            string _bucketName = ConfigurationManager.AppSettings["LIB.BucketName"];

            var client = new AmazonS3Client(_awsAccessKey, _awsSecretKey, Amazon.RegionEndpoint.APSoutheast1);

            try
            {
                PutObjectRequest putRequest = new PutObjectRequest
                {
                    CannedACL = S3CannedACL.PublicRead,
                    BucketName = _bucketName,
                    Key = fileName,
                    FilePath = filePath,
                    ContentType = "application/octet-stream"
                };

                PutObjectResponse response = client.PutObject(putRequest);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                     (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                     ||
                     amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    //throw new Exception("Check the provided AWS Credentials.");
                    LogService.CreateS3ErrorLog("Check the provided AWS Credentials.");
                }
                else
                {
                    //throw new Exception("Error occurred: " + amazonS3Exception.Message);
                    LogService.CreateS3ErrorLog("Error occurred: " + amazonS3Exception.Message);
                    return;
                }
            }
        }




        //public static IList<Product> SelectAll()
        //{
        //    try
        //    {
        //        using (RotundaEntities db = new RotundaEntities())
        //        {

        //            return db.Products.Where(x => x.Product_CategoryCode == "ARC").ToList();

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}


        //public static void UploadFileARCTEST()
        //{
        //    string destinationpath = "D:\\NGS\\Uploads\\ARC";
        //    IList<Product> res = SelectAll();
        //    foreach(Product p in res)
        //    {
        //        string filePath = Path.Combine(destinationpath, p.Product_FilePath);
        //        string fileName = p.Product_FilePath;
        //        var aa = Path.GetExtension(filePath);
        //        if(aa==".pdf")
        //        UploadFileARC(filePath, fileName);


        //    }

        //}




    }
}
