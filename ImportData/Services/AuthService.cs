﻿using ImportData.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ImportData
{
    public class AuthService
    {

        //public static string url = "https://dmz.nationalgallery.sg/r/st/Piction_login/USERNAME/OCSP/PASSWORD/OCSP/JSON/T";
        public static string url = ConfigurationManager.AppSettings["AUTH.Api.Url"];
              
        public static AUTH GetAuth()
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);//
                request.Method = "Get";
                request.KeepAlive = true;
                request.ContentType = "appication/json";               

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string myResponse = "";
                using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    myResponse = sr.ReadToEnd();
                }
                // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)
                var deserialized = JsonConvert.DeserializeObject<AUTH>(myResponse);


                return deserialized;
            }
            catch(Exception e)
            {
                LogService.CreateErrorLog("Error in calling api :"+ e.ToString());
                Console.WriteLine("Error in calling api " + e.Message);
                return null;
            }      


        }
        
       

    }
}
