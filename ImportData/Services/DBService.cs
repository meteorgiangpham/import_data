﻿using ImportData.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportData.Services
{
    public class DBService
    {      

        public static int ProductUpdateActiveForLibrary()
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spProductUpdateActiveForLibrary();

                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("Error in update product active status for library (ProductUpdateActiveForLibrary) : " + e.ToString());
                return 0;
            }
        }


        public static int SyncLogsInsert(string categoryCode,int records,int succeed,int failed)
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spSyncLogsInsert(categoryCode, records, succeed, failed);

                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("Failed importing (Error in insert sync data records). Detail error : " + e.ToString());
                return 0;
            }
        }

        public static int ProductUpdateActiveByUMOID(string umoId, bool? isActive)
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spProductUpdateActiveByUMOID(umoId,isActive);

                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("Failed importing (delete product by umoID). Detail error : " + e.ToString());
                return 0;
            }
        }


        public static int ArtworkInsert(string umoId,string filePath, string imagePath, string title, string artist, string productDate, string productLocation,string accessLevel, Nullable<int> systemId, string accessionNo, string artistDateOfBirth, string artistDateOfDeath, string creditline, string medium,
            string dimensions, string onDisplay, string photoCredit, string genere, string copyright, string exhibitionHistory, string displayLocation,string remarks, string description)
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spProductInsertArtwork(umoId,filePath, imagePath, title, artist, productDate, productLocation,accessLevel, systemId, accessionNo, artistDateOfBirth, artistDateOfDeath, creditline, medium, dimensions, onDisplay, photoCredit, genere, copyright, exhibitionHistory, displayLocation,remarks, description);

                }
            }
            catch(Exception e)
            {
                LogService.CreateErrorLog("Failed importing artwork. Detail error : " + e.ToString());
                return 0;
            }
        }

        public static int ExhibitionHistoryInsert(string umoId, string decision, string displayLocation, string title)
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spExhibitionHistoryInsert(umoId,decision,displayLocation,title);

                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("Failed importing artwork. Detail error : " + e.ToString());
                return 0;
            }
        }


        public static int ArchiveInsert(string uMOId,string filePath, string imagePath,
            string title, string artist, string productDate, string productLocation, 
            string accessLevel, string referenceCode, string otherTitle, 
            string fondsCollection, string typeOfDate, string language, string subject,
            string materialType, string extentAndMedium, string storageLocation,
            string conditionsGoverningAccess, string usageRestrictionsCode, 
            string conditionsGoverningUseAndReproduction, string systemArrangement,
            string archivalHistory, string sourceOfAcquisition, string locationOfOriginals,
            string locationOfCopies, string notes, string scopeAndContent, string levelOfDetail,
            string levelOfDescription, string status,string subjectPeronName)
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spProductInsertArchive(uMOId,filePath, imagePath, title, artist, productDate, productLocation, accessLevel, referenceCode, otherTitle, fondsCollection, typeOfDate, language, subject, materialType, extentAndMedium, storageLocation.Trim(), conditionsGoverningAccess
                        , usageRestrictionsCode, conditionsGoverningUseAndReproduction, systemArrangement, archivalHistory, sourceOfAcquisition, locationOfOriginals, locationOfCopies, notes, scopeAndContent, levelOfDetail, levelOfDescription, status,subjectPeronName);

                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("Failed importing archives. Detail error : " + e.ToString());
                return 0;
            }
        }


        public static int LibraryItemInsert(string barcode, string bRN, string location, string collection, 
            string category1, string category2, string classNumber, string suffix, string prefix, string status, string statusDescription, 
            Nullable<System.DateTime> statusDate, string availability, bool isActive)
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spProductInsertLibraryItem(barcode,bRN,location,collection,category1,category2
                        ,classNumber,suffix,prefix,status,statusDescription,statusDate,availability,isActive);

                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("Failed importing Library item. Detail error : " + e.ToString());
                return 0;
            }
        }


        public static int LibraryInsert(string filePath, string imagePath, string title, string artist, string productDate, string productLocation, string accessLevel, string bRN, string author, string corporateAuthor, string meetingName, string editionStatement, string imprint, string collation, string seriesTitle, string notes, string linkingNotes, string credits, string performers, string systemDetails, string contents, string summary, string iSBN, string iSSN, string deweyClass, string language, string variantTitle, string addedTitle, string subject, string datePublished, string bibFormat, string iRN, string hostItemEntry, Nullable<System.DateTime> created, Nullable<System.DateTime> lastUpdated)
        {
            try
            {
                using (RotundaEntities db = new RotundaEntities())
                {

                    return db.spProductInsertLibrary(filePath, imagePath, title, artist, productDate, productLocation, accessLevel
                        , bRN, author, corporateAuthor, meetingName, editionStatement, imprint, collation, seriesTitle, notes, linkingNotes
                        , credits, performers, systemDetails, contents, summary, iSBN, iSSN, deweyClass, language, variantTitle, addedTitle, subject, datePublished
                        , bibFormat, iRN, hostItemEntry, created, lastUpdated);

                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog("Failed importing Library Bib. Detail error : " + e.ToString());
                return 0;
            }
        }

        #region Libary

        public static int LibrarySyncData(BiBDetail l, string imagePathInput)
        {
            using (RotundaEntities db = new RotundaEntities())
            {
                string imagePath = string.Empty;
                string Title = string.Empty;
                string personalAuthor = string.Empty;
                string Artist = string.Empty;
                string corporateAuthor = string.Empty;
                string meetingName = string.Empty;
                string imprint = string.Empty;
                string collation = string.Empty;
                string seriesTitle = string.Empty;
                string linkedNotes = string.Empty;
                string subject = string.Empty;
                string accessLevel = "Unrestricted";

                imagePath = imagePathInput;
                if (l.Country == "||") l.Country = null;
                //***   
                Title = (!string.IsNullOrEmpty(l.Title) ? l.Title : "")
                    + (!string.IsNullOrEmpty(l.Medium) ? " " + l.Medium : "")
                    + (!string.IsNullOrEmpty(l.RemainderofTitle) ? " : " + l.RemainderofTitle : "")
                    + (!string.IsNullOrEmpty(l.StatementofResponsibility) ? " / " + l.StatementofResponsibility : "");
                //***   
                personalAuthor = (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? l.MainEntryPersonalName : "")
                                + (!string.IsNullOrEmpty(l.AddedEntry_PersonalName) ? (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? "||" : "") + l.AddedEntry_PersonalName : "");
                //***   
                corporateAuthor = (!string.IsNullOrEmpty(l.MainEntryCorporateName) ? l.MainEntryCorporateName + "||" : "")
                              + (!string.IsNullOrEmpty(l.AddedEntry_CorporateName) ? l.AddedEntry_CorporateName : "");
                //***
                Artist = (!string.IsNullOrEmpty(personalAuthor) ? personalAuthor : "")
                             + (!string.IsNullOrEmpty(corporateAuthor) ? (!string.IsNullOrEmpty(personalAuthor) ? "||" : "") + corporateAuthor : "");
                //***
                meetingName = (!string.IsNullOrEmpty(l.MeetingName) ? l.MeetingName : "")
                             + (!string.IsNullOrEmpty(l.AddedEntry_MeetingName) ? (!string.IsNullOrEmpty(l.MeetingName) ? "||" : "") + l.AddedEntry_MeetingName : "");
                //***   
                imprint = (!string.IsNullOrEmpty(l.PlaceofPublication)) ? l.PlaceofPublication + " : " + l.NameofPublisher + ", " + l.DateofPublication : " ";
                //***   
                collation = (!string.IsNullOrEmpty(l.Collation_Extent)) ? l.Collation_Extent + " : " + l.Collation_OtherPhysicalDetails + " ; " + l.Collation_Dimensions + "  " + l.Collation_Accompanying : "";
                //***
                seriesTitle = (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? l.SeriesStatement_Title : "")
                             + (!string.IsNullOrEmpty(l.SeriesStatement_Volumenumber) ? (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? " ; " : "") + l.SeriesStatement_Volumenumber : "");
                //***
                linkedNotes = (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? l.Linkingnotes_PrecedingEntry : "")
                             + (!string.IsNullOrEmpty(l.Linkingnotes_SucceedingEntry) ? (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? "||" : "") + l.Linkingnotes_SucceedingEntry : "");
                //***
                subject = (!string.IsNullOrEmpty(l.SubjectAddedEntry_TopicalTerm) ? l.SubjectAddedEntry_TopicalTerm + "||" : "")
                    + (!string.IsNullOrEmpty(l.SubjectAddedEntry_PersonalName) ? l.SubjectAddedEntry_PersonalName + "||" : "")
                    + (!string.IsNullOrEmpty(l.SubjectAddedEntry_CorporateName) ? l.SubjectAddedEntry_CorporateName + "||" : "")
                    + (!string.IsNullOrEmpty(l.SubjectAddedEntry_MeetingName) ? l.SubjectAddedEntry_MeetingName + "||" : "");
                //***
                DateTime temp = new DateTime();
                DateTime? CreatedDateTime = null;
                if (!string.IsNullOrEmpty(l.CreatedDateTime))
                {
                    if (DateTime.TryParse(l.CreatedDateTime, out temp))
                    {
                        CreatedDateTime = DateTime.Parse(l.CreatedDateTime);
                    }
                }
                //***
                DateTime? LastUpdatedDateTime = null;
                if (!string.IsNullOrEmpty(l.LastUpdatedDateTime))
                {
                    if (DateTime.TryParse(l.LastUpdatedDateTime, out temp))
                    {
                        LastUpdatedDateTime = DateTime.Parse(l.LastUpdatedDateTime);
                    }
                }

                return db.spProductInsertLibrary(null, imagePath, Title, Artist, l.DateofPublication,
                                l.Country, accessLevel, l.BRN, personalAuthor, corporateAuthor, meetingName,
                                l.EditionStatement, imprint, collation, seriesTitle, l.GeneralNote, linkedNotes,
                                l.CreationorProductionCreditsNote, l.ParticipantorPerformerNote, l.SystemDetailsNote, l.FormattedContentsNote, l.Summary, l.ISBN, l.ISSN,
                                l.DeweyClass, l.Language, l.VaryingformofTitle, l.RemainderofTitle2, subject, l.DateofPublication,
                                null, l.IRN.ToString(), l.HostItemEntry, CreatedDateTime, LastUpdatedDateTime);
            }
        }

        #endregion Libary

        #region LibaryItems

        public static int LibraryItemsSyncData(ItemDetail item)
        {
            using (RotundaEntities db = new RotundaEntities())
            {
                DateTime temp = new DateTime();
                DateTime? StatusSetDate = null;
                if (!string.IsNullOrEmpty(item.StatusSetDate))
                {
                    if (DateTime.TryParse(item.StatusSetDate, out temp))
                    {
                        StatusSetDate = DateTime.Parse(item.StatusSetDate);
                    }
                }
                DateTime? CreatedDateTime = null;
                if (!string.IsNullOrEmpty(item.CreatedDateTime))
                {
                    if (DateTime.TryParse(item.CreatedDateTime, out temp))
                    {
                        CreatedDateTime = DateTime.Parse(item.CreatedDateTime);
                    }
                }
                DateTime? LastUpdatedDateTime = null;
                if (!string.IsNullOrEmpty(item.LastUpdatedDateTime))
                {
                    if (DateTime.TryParse(item.LastUpdatedDateTime, out temp))
                    {
                        LastUpdatedDateTime = DateTime.Parse(item.LastUpdatedDateTime);
                    }
                }
                return db.spLibraryItemsSyncData(item.IRN.ToString(), item.BibIrn, 0, item.Barcode, item.BRN, item.Location, item.CollectionCode, item.Category1, item.Category2, item.ClassNumber, item.Suffix, item.Prefix, item.ItemStatus, "", StatusSetDate, item.Availability, true, CreatedDateTime, LastUpdatedDateTime);
            }
        }

        #endregion LibaryItems

        #region HistorySyncCivica

        public static spGetDataSyncCivicaFailure_Result[] GetDataSyncCivicaFailure(string typeSync)
        {
            using (RotundaEntities db = new RotundaEntities())
            {
                return db.spGetDataSyncCivicaFailure(typeSync).ToArray();
            }
        }

        public static int UpdateSyncCivicaHistory(SyncCivica item)
        {
            using (RotundaEntities db = new RotundaEntities())
            {
                return db.spUpdateSyncCivicaHistory(item.Id, item.TypeSync, item.Dt, item.Dur, item.Message, item.Status, item.StatusUpdate, item.UpdateTime);
            }
        }

        #endregion HistorySyncCivica
    }
}
