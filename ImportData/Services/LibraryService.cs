﻿using ImportData.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Http;
using ImportData.Models;
using System.Net.Http.Headers;

namespace ImportData
{
    public class LibraryService
    {

        static string destinationpath = ConfigurationManager.AppSettings["LIB.Download.Path"];
        static string libPath = ConfigurationManager.AppSettings["LIB.FTP.Path"];
        static string civicaApi = ConfigurationManager.AppSettings["civica.api"];
        static string civicaUsername = ConfigurationManager.AppSettings["civica.username"];
        static string civicaPassword = ConfigurationManager.AppSettings["civica.password"];
        public static void ImportLibraryData()
        {
            try
            {

                string biblio = Path.Combine(libPath, "Biblio.json");

                JArray o1 = JArray.Parse(File.ReadAllText(biblio));

                List<LibraryBIB> result = JsonConvert.DeserializeObject<List<LibraryBIB>>(o1.ToString());

                string imagePath = string.Empty;
                string Title = string.Empty;
                string personalAuthor = string.Empty;
                string Artist = string.Empty;
                string corporateAuthor = string.Empty;
                string meetingName = string.Empty;
                string imprint = string.Empty;
                string collation = string.Empty;
                string seriesTitle = string.Empty;
                string linkedNotes = string.Empty;
                string subject = string.Empty;
                string accessLevel = "Unrestricted";
                int succeed = 0;
                int failed = 0;

                foreach (LibraryBIB l in result)
                {
                    imagePath = DownloadFile(l.BookCoverMedium);

                    if (l.Country == "||") l.Country = null;

                    //Title = l.Title + l.RemainderofTitle + l.StatementofResponsibility + l.Medium;

                    //***   
                    Title = (!string.IsNullOrEmpty(l.Title) ? l.Title : "")
                        + (!string.IsNullOrEmpty(l.Medium) ? " " + l.Medium : "")
                        + (!string.IsNullOrEmpty(l.RemainderofTitle) ? " : " + l.RemainderofTitle : "")
                        + (!string.IsNullOrEmpty(l.StatementofResponsibility) ? " / " + l.StatementofResponsibility : "");
                    //Title = (!string.IsNullOrEmpty(l.Title) ? l.Title : "")
                    //    + (!string.IsNullOrEmpty(l.Medium) ? " " + l.Medium + " : " : "")
                    //    + (!string.IsNullOrEmpty(l.RemainderofTitle)?(string.IsNullOrEmpty(l.Medium) ?" : ": "") + l.RemainderofTitle : "")
                    //    + (!string.IsNullOrEmpty(l.StatementofResponsibility) ? (!string.IsNullOrEmpty(l.RemainderofTitle) ? " / ":"") + l.StatementofResponsibility : "");


                    personalAuthor = (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? l.MainEntryPersonalName : "")
                                    + (!string.IsNullOrEmpty(l.AddedEntry_PersonalName) ? (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? "||" : "") + l.AddedEntry_PersonalName : "");

                    corporateAuthor = (!string.IsNullOrEmpty(l.MainEntryCorporateName) ? l.MainEntryCorporateName + "||" : "")
                                  + (!string.IsNullOrEmpty(l.AddedEntry_CorporateName) ? l.AddedEntry_CorporateName : "");

                    //personalAuthor = l.MainEntryPersonalName + l.AddedEntry_PersonalName;
                    //corporateAuthor = l.MainEntryCorporateName + l.AddedEntry_CorporateName;
                    //Artist = personalAuthor + Environment.NewLine + corporateAuthor;

                    //***
                    Artist = (!string.IsNullOrEmpty(personalAuthor) ? personalAuthor : "")
                                 + (!string.IsNullOrEmpty(corporateAuthor) ? (!string.IsNullOrEmpty(personalAuthor) ? "||" : "") + corporateAuthor : "");

                    //meetingName = l.MeetingName + l.AddedEntry_MeetingName;

                    //***
                    meetingName = (!string.IsNullOrEmpty(l.MeetingName) ? l.MeetingName : "")
                                 + (!string.IsNullOrEmpty(l.AddedEntry_MeetingName) ? (!string.IsNullOrEmpty(l.MeetingName) ? "||" : "") + l.AddedEntry_MeetingName : "");

                    //meetingName = l.MeetingName + l.AddedEntry_MeetingName;

                    //imprint = l.PlaceofPublication + l.NameofPublisher + l.DateofPublication;

                    imprint = (!string.IsNullOrEmpty(l.PlaceofPublication)) ? l.PlaceofPublication + " : " + l.NameofPublisher + ", " + l.DateofPublication : " ";

                    //"Place of publication" : "Name of publisher", "Date of publication".
                    //"Collation - extent" : "collation - other physical details"; "collation - dimension" + "collation - accompanying material"

                    collation = (!string.IsNullOrEmpty(l.Collation_Extent)) ? l.Collation_Extent + " : " + l.Collation_OtherPhysicalDetails + " ; " + l.Collation_Dimensions + "  " + l.Collation_Accompanying : "";

                    //seriesTitle = l.SeriesStatement_Title + l.SeriesStatement_Volumenumber;
                    //***
                    seriesTitle = (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? l.SeriesStatement_Title : "")
                                 + (!string.IsNullOrEmpty(l.SeriesStatement_Volumenumber) ? (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? " ; " : "") + l.SeriesStatement_Volumenumber : "");

                    //linkedNotes = l.Linkingnotes_PrecedingEntry + l.Linkingnotes_SucceedingEntry;
                    //***
                    linkedNotes = (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? l.Linkingnotes_PrecedingEntry : "")
                                 + (!string.IsNullOrEmpty(l.Linkingnotes_SucceedingEntry) ? (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? ";" : "") + l.Linkingnotes_SucceedingEntry : "");



                    subject = (!string.IsNullOrEmpty(l.SubjectAddedEntry_TopicalTerm) ? l.SubjectAddedEntry_TopicalTerm + "||" : "")
                        + (!string.IsNullOrEmpty(l.SubjectAddedEntry_PersonalName) ? l.SubjectAddedEntry_PersonalName + "||" : "")
                        + (!string.IsNullOrEmpty(l.SubjectAddedEntry_CorporateName) ? l.SubjectAddedEntry_CorporateName + "||" : "")
                        + (!string.IsNullOrEmpty(l.SubjectAddedEntry_MeetingName) ? l.SubjectAddedEntry_MeetingName + "||" : "");

                    if (!String.IsNullOrEmpty(l.Title) && !String.IsNullOrEmpty(l.BRN))
                    {

                        int libResult = DBService.LibraryInsert(null, imagePath, Title, Artist, l.DateofPublication,
                            l.Country, accessLevel, l.BRN, personalAuthor, corporateAuthor, meetingName,
                            l.EditionStatement, imprint, collation, seriesTitle, l.GeneralNote, linkedNotes,
                            l.CreationorProductionCreditsNote, l.ParticipantorPerformerNote, l.SystemDetailsNote, l.FormattedContentsNote, l.Summary, l.ISBN, l.ISSN,
                            l.DeweyClass, l.Language, l.VaryingformofTitle, l.RemainderofTitle2, subject, l.DateofPublication,
                            null, l.IRN, l.HostItemEntry, l.CreatedDate, l.UpdatedDate);

                        if (libResult > 0) succeed += 1;
                        else failed += 1;

                    }
                    else
                    {
                        LogService.CreateFailImportLog("No Title & No BRN");
                        failed += 1;
                    }




                }

                #region Sync Log
                DBService.SyncLogsInsert("LIB", result.Count(), succeed, failed);
                #endregion
            }
            catch (FileNotFoundException e)
            {
                LogService.CreateErrorLog("LIB Biblio file is not found. Detail error : " + e.Message);

            }
        }

        public static async  Task ImportLibraryDataFromCivicaApi(string option, string fromDate, string toDate, string irn)
        {
            string dt = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
            SyncResponse res = new SyncResponse();
            try
            {
                LogService.CreateImportActivityLog("Start sync ImportLibraryDataFromCivicaApi: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
                res.status = true;
                if (string.IsNullOrEmpty(option))
                {
                    //get sync history failure
                    spGetDataSyncCivicaFailure_Result[] listSyncFailure = DBService.GetDataSyncCivicaFailure("bib");
                    if (listSyncFailure != null && listSyncFailure.Length > 0)
                    {
                        for (int i = 0; i < listSyncFailure.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(listSyncFailure[i].Dt))
                            {
                                await SyncBiBs(listSyncFailure[i].Id, listSyncFailure[i].Dt);
                            }
                        }
                    }
                    await SyncBiBs(0, dt);
                }
                else
                {
                    if(option == "1")
                    {
                        DateTime temp;
                        DateTime fromDateTime;
                        DateTime toDateTime;
                        if(DateTime.TryParse(fromDate, out temp) && DateTime.TryParse(toDate, out temp))
                        {
                            fromDateTime = DateTime.Parse(fromDate);
                            toDateTime = DateTime.Parse(toDate);
                            LogService.CreateFailImportLog("fromDateTime " + fromDateTime + " toDateTime " + toDateTime);
                            int countDay = toDateTime.Subtract(fromDateTime).Days;
                            for(int i = 0; i < countDay; i++)
                            {
                                await SyncBiBs(0, fromDateTime.AddDays(i).ToString("dd/MM/yyyy"));
                            }
                        }
                    }
                    else if(option == "2")
                    {
                        if (!string.IsNullOrEmpty(irn))
                        {
                            await SyncBiBById(irn);
                        }
                    }
                }
                LogService.CreateImportActivityLog("End sync ImportLibraryDataFromCivicaApi: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
            }
            catch (Exception ex)
            {
                LogService.CreateImportActivityLog("End sync ImportLibraryDataFromCivicaApi: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
                LogService.CreateFailImportLog("Import Library Data From Civica Api failure: " + "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message);
                res.status = false;
                res.message = "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message;
                UpdateSyncCivicaHistory(0, "bib", dt, res.message, 0, res.status);
            }
        }

        public static async Task<SyncResponse> SyncBiBs(long idSync, string dt)
        {
            SyncResponse res = new SyncResponse();
            try
            {
                res.status = true;
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "password" },
                    { "username", civicaUsername },
                    { "password", civicaPassword }
                };
                var content = new FormUrlEncodedContent(values);
                var loginResponse = await client.PostAsync(civicaApi + "/ngapi/token", content);
                var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
                LoginCivicaResponse loginRes = JsonConvert.DeserializeObject<LoginCivicaResponse>(loginResponseString);
                if (loginRes != null && !string.IsNullOrEmpty(loginRes.access_token))
                {
                    string token = loginRes.access_token;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var bibListResponse = await client.PostAsync(civicaApi + "/ngapi/bib/list?dt=" + dt, null);
                    var bibListResponseString = await bibListResponse.Content.ReadAsStringAsync();
                    SyncBiBListData bibRes = JsonConvert.DeserializeObject<SyncBiBListData>(bibListResponseString);
                    if (bibRes.status == "SUCCESS")
                    {
                        if (bibRes.data != null && bibRes.data.Count() > 0)
                        {
                            var dataFailure = new List<BiBItem>();
                            var dataSuccess = new List<BiBItem>();
                            int countSuccess = 0;
                            foreach (var bibItem in bibRes.data)
                            {
                                bool isSuccess = false;
                                var bibDetailResponse = await client.GetAsync(civicaApi + "/ngapi/bib/" + bibItem.irn.ToString());
                                var bibDetailResponseString = await bibDetailResponse.Content.ReadAsStringAsync();
                                SyncBiBDetailData item = JsonConvert.DeserializeObject<SyncBiBDetailData>(bibDetailResponseString);
                                if (item.status == "SUCCESS")
                                {
                                    if(item.data != null)
                                    {
                                        string imagePath = string.Empty;
                                        if (!string.IsNullOrEmpty(item.data.BookCoverMedium))
                                        {
                                            imagePath = DownloadFile(item.data.BookCoverMedium);
                                        }
                                        int resSync = DBService.LibrarySyncData(item.data, imagePath);
                                        if (resSync > 0)
                                        {
                                            countSuccess = countSuccess + 1;
                                            isSuccess = true;
                                        }
                                    }
                                    else
                                    {
                                        countSuccess = countSuccess + 1;
                                        isSuccess = true;
                                    }
                                }
                                if (!isSuccess)
                                {
                                    dataFailure.Add(bibItem);
                                }
                                else
                                {
                                    dataSuccess.Add(bibItem);
                                }
                            }
                            res.message = "Data sync is finished."
                                          + "- Total data sync:" + bibRes.data.Count().ToString()
                                          + "- Total data sync successfully:" + countSuccess.ToString()
                                          + "- Total data sync failure: " + (bibRes.data.Count() - countSuccess).ToString();

                            string str = "Data SyncBiBs is finished."
                                          + "- Dt: " + dt
                                          + "- List data sync successfully: " + JsonConvert.SerializeObject(dataSuccess)
                                          + "- List data sync failure: " + JsonConvert.SerializeObject(dataFailure);
                            LogService.CreateImportActivityLog(str);

                            if (bibRes.data.Count() - countSuccess > 0)
                            {
                                res.status = false;
                            }
                        }
                        else
                        {
                            res.message = "No records need to be updated";
                        }
                    }
                    else if (bibRes.status == "FAIL" && bibRes.msg == "Data not found")
                    {
                        res.message = "No records need to be updated";
                    }
                    else
                    {
                        res.status = false;
                        res.message = "Get data BiB list failure." + bibRes.msg;
                    }
                }
                else
                {
                    res.status = false;
                    res.message = "Login failure.";
                }
                UpdateSyncCivicaHistory(idSync, "bib", dt, res.message, 0, res.status);
                return res;
            }
            catch (Exception ex)
            {
                LogService.CreateFailImportLog("Sync BiBs failure: " + "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message);
                res.status = false;
                res.message = "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message;
                UpdateSyncCivicaHistory(idSync, "bib", dt, res.message, 0, res.status);
                return res;
            }
        }

        public static async Task<SyncResponse> SyncBiBById(string irn)
        {
            SyncResponse res = new SyncResponse();
            try
            {
                res.status = true;
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "password" },
                    { "username", civicaUsername },
                    { "password", civicaPassword }
                };
                var content = new FormUrlEncodedContent(values);
                var loginResponse = await client.PostAsync(civicaApi + "/ngapi/token", content);
                var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
                LoginCivicaResponse loginRes = JsonConvert.DeserializeObject<LoginCivicaResponse>(loginResponseString);
                if (loginRes != null && !string.IsNullOrEmpty(loginRes.access_token))
                {
                    string token = loginRes.access_token;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    int countSuccess = 0;
                    var bibDetailResponse = await client.GetAsync(civicaApi + "/ngapi/bib/" + irn.ToString());
                    var bibDetailResponseString = await bibDetailResponse.Content.ReadAsStringAsync();
                    SyncBiBDetailData item = JsonConvert.DeserializeObject<SyncBiBDetailData>(bibDetailResponseString);
                    if (item.status == "SUCCESS" && item.data != null)
                    {
                        string imagePath = string.Empty;
                        if (!string.IsNullOrEmpty(item.data.BookCoverMedium))
                        {
                            imagePath = DownloadFile(item.data.BookCoverMedium);
                        }
                        int resSync = DBService.LibrarySyncData(item.data, imagePath);
                        if (resSync > 0)
                        {
                            countSuccess = 1;
                        }
                        res.message = "Data sync is finished."
                                      + "Data sync IRN: " + irn
                                      + "- Total data sync: 1"
                                      + "- Total data sync successfully:" + countSuccess.ToString()
                                      + "- Total data sync failure: " + (1 - countSuccess).ToString()
                                      + "- List data sync: " + JsonConvert.SerializeObject(item.data);
                    }
                    else
                    {
                        res.message = "Data sync is finished."
                                      + "- No records need to be updated for irn: " + irn;
                    }
                }
                else
                {
                    res.status = false;
                    res.message = "Login failure.";
                }
                UpdateSyncCivicaHistory(0, "bib", irn, res.message, 0, res.status);
                return res;
            }
            catch (Exception ex)
            {
                LogService.CreateFailImportLog("Sync BiBs failure: " + "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message);
                res.status = false;
                res.message = "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message;
                UpdateSyncCivicaHistory(0, "bib", irn, res.message, 0, res.status);
                return res;
            }
        }

        public static void ImportLibraryDataUpdate()
        {
            try
            {

                string todayDate = DateTime.Now.AddDays(-1).ToString("ddMMyyyy");              

                string filePath = "Biblio_" + todayDate+".json" ;

                LogService.CreateImportActivityLog("Import biblio file  :" + filePath + "   |    Current DateTime : "+ DateTime.Now.ToString());

                string biblio = Path.Combine(libPath,filePath);

                JArray o1 = JArray.Parse(File.ReadAllText(biblio));
               

                List<LibraryBIB> result = JsonConvert.DeserializeObject<List<LibraryBIB>>(o1.ToString());

                string imagePath = string.Empty;
                string Title = string.Empty;
                string personalAuthor = string.Empty;
                string Artist = string.Empty;
                string corporateAuthor = string.Empty;
                string meetingName = string.Empty;
                string imprint = string.Empty;
                string collation = string.Empty;
                string seriesTitle = string.Empty;
                string linkedNotes = string.Empty;
                string subject = string.Empty;
                string accessLevel = "Unrestricted";
                int succeed = 0;
                int failed = 0;

                foreach (LibraryBIB l in result)
                {                   
                    if (l.UpdatedDate > DateTime.Now.AddDays(-5))
                    {

                        imagePath = DownloadFile(l.BookCoverMedium);

                        if (l.Country == "||") l.Country = null;


                        //***   
                        Title = (!string.IsNullOrEmpty(l.Title) ? l.Title : "")
                            + (!string.IsNullOrEmpty(l.Medium) ? " " + l.Medium : "")
                            + (!string.IsNullOrEmpty(l.RemainderofTitle) ? " : " + l.RemainderofTitle : "")
                            + (!string.IsNullOrEmpty(l.StatementofResponsibility) ? " / " + l.StatementofResponsibility : "");



                        personalAuthor = (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? l.MainEntryPersonalName : "")
                                        + (!string.IsNullOrEmpty(l.AddedEntry_PersonalName) ? (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? "||" : "") + l.AddedEntry_PersonalName : "");

                        corporateAuthor = (!string.IsNullOrEmpty(l.MainEntryCorporateName) ? l.MainEntryCorporateName + "||" : "")
                                      + (!string.IsNullOrEmpty(l.AddedEntry_CorporateName) ? l.AddedEntry_CorporateName : "");



                        //***
                        Artist = (!string.IsNullOrEmpty(personalAuthor) ? personalAuthor : "")
                                     + (!string.IsNullOrEmpty(corporateAuthor) ? (!string.IsNullOrEmpty(personalAuthor) ? "||" : "") + corporateAuthor : "");



                        //***
                        meetingName = (!string.IsNullOrEmpty(l.MeetingName) ? l.MeetingName : "")
                                     + (!string.IsNullOrEmpty(l.AddedEntry_MeetingName) ? (!string.IsNullOrEmpty(l.MeetingName) ? "||" : "") + l.AddedEntry_MeetingName : "");



                        imprint = (!string.IsNullOrEmpty(l.PlaceofPublication)) ? l.PlaceofPublication + " : " + l.NameofPublisher + ", " + l.DateofPublication : " ";



                        collation = (!string.IsNullOrEmpty(l.Collation_Extent)) ? l.Collation_Extent + " : " + l.Collation_OtherPhysicalDetails + " ; " + l.Collation_Dimensions + "  " + l.Collation_Accompanying : "";


                        //***
                        seriesTitle = (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? l.SeriesStatement_Title : "")
                                     + (!string.IsNullOrEmpty(l.SeriesStatement_Volumenumber) ? (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? " ; " : "") + l.SeriesStatement_Volumenumber : "");

                        //linkedNotes = l.Linkingnotes_PrecedingEntry + l.Linkingnotes_SucceedingEntry;
                        //***
                        linkedNotes = (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? l.Linkingnotes_PrecedingEntry : "")
                                     + (!string.IsNullOrEmpty(l.Linkingnotes_SucceedingEntry) ? (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? "||" : "") + l.Linkingnotes_SucceedingEntry : "");



                        subject = (!string.IsNullOrEmpty(l.SubjectAddedEntry_TopicalTerm) ? l.SubjectAddedEntry_TopicalTerm + "||" : "")
                            + (!string.IsNullOrEmpty(l.SubjectAddedEntry_PersonalName) ? l.SubjectAddedEntry_PersonalName + "||" : "")
                            + (!string.IsNullOrEmpty(l.SubjectAddedEntry_CorporateName) ? l.SubjectAddedEntry_CorporateName + "||" : "")
                            + (!string.IsNullOrEmpty(l.SubjectAddedEntry_MeetingName) ? l.SubjectAddedEntry_MeetingName + "||" : "");

                        if (!String.IsNullOrEmpty(l.Title) && !String.IsNullOrEmpty(l.BRN))
                        {

                            int libResult = DBService.LibraryInsert(null, imagePath, Title, Artist, l.DateofPublication,
                                l.Country, accessLevel, l.BRN, personalAuthor, corporateAuthor, meetingName,
                                l.EditionStatement, imprint, collation, seriesTitle, l.GeneralNote, linkedNotes,
                                l.CreationorProductionCreditsNote, l.ParticipantorPerformerNote, l.SystemDetailsNote, l.FormattedContentsNote, l.Summary, l.ISBN, l.ISSN,
                                l.DeweyClass, l.Language, l.VaryingformofTitle, l.RemainderofTitle2, subject, l.DateofPublication,
                                null, l.IRN, l.HostItemEntry, l.CreatedDate, l.UpdatedDate);

                            if (libResult > 0) succeed += 1;
                            else failed += 1;

                        }
                        else
                        {
                            LogService.CreateFailImportLog("No Title & No BRN");
                            failed += 1;
                        }
                    }




                }

                #region Sync Log
                DBService.SyncLogsInsert("LIB", result.Count(), succeed, failed);
                #endregion

                 #region Old File Delete
                string[] files = Directory.GetFiles(libPath);

                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);

                    if (fi.LastAccessTime < DateTime.Now.AddDays(-15))
                    {
                        fi.Delete();
                    }
                }
                #endregion

               
            }
            catch (FileNotFoundException e)
            {
                LogService.CreateErrorLog("LIB Biblio file is not found. Detail error : " + e.Message);

            }
        }

        private static string DownloadFile(string url)
        {
            try
            {
                //return null;

                if (url == "http://www.bibdsl.co.uk/xmla/image-service.asp?ISBN=&SIZE=m&DBM=NAG") return null;

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                string filename = "";
               
                if (!Directory.Exists(destinationpath))
                {
                    Directory.CreateDirectory(destinationpath);
                }


                using (HttpWebResponse response1 = (HttpWebResponse)request.GetResponseAsync().Result)
                {
                    request = (HttpWebRequest)HttpWebRequest.Create(response1.ResponseUri.ToString());
                    HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result;
                    string path = response.Headers["Content-Disposition"];
                    if (string.IsNullOrWhiteSpace(path))
                    {
                        var uri = new Uri(response1.ResponseUri.ToString());
                        filename = Path.GetFileName(uri.LocalPath);
                    }
                    else
                    {
                        ContentDisposition contentDisposition = new ContentDisposition(path);
                        filename = contentDisposition.FileName;

                    }


                    var filepath = Path.Combine(destinationpath, filename);
                   

                    var responseStream = response.GetResponseStream();
                    using (var fileStream = File.Create(System.IO.Path.Combine(destinationpath, filename)))
                    {
                        responseStream.CopyTo(fileStream);
                    }

                    if (filename == "noimage-m.png") return null;

                    UploadToS3.UploadFileLibrary(filepath, filename);

                    //Delete from EC2
                    if (File.Exists(filepath))
                    {
                        File.Delete(filepath);
                    }
                }               

                return filename;

               
            }


            catch (Exception e)
            {
                LogService.CreateFailImportLog("File download failed: url" + url + " Detail: " + e.ToString());
                return null;
            }
        }

        public static void ImportLibraryItemDataUpdate()
        {
            try
            {
                libPath = "D:\\NGS\\Library";
                var directory = new DirectoryInfo(libPath);

                // or...
                var myFile = directory.GetFiles("Item_*")
                             .OrderByDescending(f => f.LastWriteTime)
                             .First();


                string filePath = Path.Combine(libPath, myFile.ToString());

                LogService.CreateImportActivityLog("Import item file  : " + filePath + " |    Current DateTime : " + DateTime.Now.ToString());
              

                JArray o1 = JArray.Parse(File.ReadAllText(filePath));


                List<LibraryItems> result = JsonConvert.DeserializeObject<List<LibraryItems>>(o1.ToString());

                bool isActive = true;

                foreach (LibraryItems l in result)
                {
                    isActive = true; // Initialize

                    if (String.IsNullOrEmpty(l.BRN))
                    {
                        LogService.CreateFailImportLog("No BRN For :" + l.Barcode);
                        continue;
                    }

                    if (l.ItemStatus.Contains("Missing"))
                        isActive = false;

                    if (l.CollectionCode.Contains("Auction Catalogues") || l.CollectionCode.Contains("Publications Reserve") || l.CollectionCode.Contains("Digitised Collection"))
                        isActive = false;

                    LogService.CreateImportActivityLog("Item data  : " + JsonConvert.SerializeObject(l) + " |    Current DateTime : " + DateTime.Now.ToString());

                    DBService.LibraryItemInsert(l.Barcode, l.BRN, l.Location, l.CollectionCode, l.Category1, l.Category2, l.ClassNumber, l.Suffix, l.Prefix, l.ItemStatus, l.Description, l.StatusDate, l.Availability, isActive);

                }
                return;
                              



            }
            catch (Exception e)
            {
                LogService.CreateFailImportLog("Failed import item : Detail info: " + e.ToString());
                return;
            }


        }

        //public static void ImportLibraryItemDataToTest()
        //{
        //    try
        //    {


        //        string todayDate = DateTime.Now.ToString("ddMMyyyyHH");

        //        todayDate = "0310201913";
        //        string item = "Item_" + todayDate + "*";

        //        LogService.CreateImportActivityLog(todayDate + " item file info :" + item);

        //        string[] matches = Directory.GetFiles(libPath, item);

        //        if (matches != null && matches.OrderBy(x => x.ToString()).Count() > 0)
        //        {
        //            string aa = matches.OrderByDescending(x => x.ToString()).FirstOrDefault().ToString();
        //        }

        //        string items = Path.Combine(libPath, "Item.json");

        //        JArray o1 = JArray.Parse(File.ReadAllText(items));



        //        List<LibraryItems> result = JsonConvert.DeserializeObject<List<LibraryItems>>(o1.ToString());

        //        bool isActive = true;

        //        foreach (LibraryItems l in result)
        //        {
        //            isActive = true; // Initialize

        //            if (String.IsNullOrEmpty(l.BRN))
        //            {
        //                LogService.CreateFailImportLog("No BRN For :" + l.Barcode);
        //            }

        //            if (l.ItemStatus.Contains("Missing"))
        //                isActive = false;

        //            if (l.CollectionCode.Contains("Auction Catalogues") || l.CollectionCode.Contains("Publications Reserve") || l.CollectionCode.Contains("Digitised Collection"))
        //                isActive = false;

        //            DBService.LibraryItemInsert(l.Barcode, l.BRN, l.Location, l.CollectionCode, l.Category1, l.Category2, l.ClassNumber, l.Suffix, l.Prefix, l.ItemStatus, l.Description, l.StatusDate, l.Availability, isActive);

        //        }

        //        DBService.ProductUpdateActiveForLibrary();

        //    }
        //    catch (Exception e)
        //    {
        //        LogService.CreateFailImportLog("Failed import item : Detail info: " + e.ToString());
        //        return;
        //    }


        //}

        public static void ImportLibraryDataUpdateToTest()
        {
            try
            {

                string todayDate = DateTime.Now.AddDays(-1).ToString("ddMMyyyy");

                string filePath = "Biblio_" + todayDate + ".json";

                LogService.CreateImportActivityLog("Import biblio file  :" + filePath + "   |    Current DateTime : " + DateTime.Now.ToString());

                string biblio = Path.Combine(libPath, filePath);

                biblio = Path.Combine("D:\\NGS", filePath);

                JArray o1 = JArray.Parse(File.ReadAllText(biblio));


                List<LibraryBIB> result = JsonConvert.DeserializeObject<List<LibraryBIB>>(o1.ToString());

                string imagePath = string.Empty;
                string Title = string.Empty;
                string personalAuthor = string.Empty;
                string Artist = string.Empty;
                string corporateAuthor = string.Empty;
                string meetingName = string.Empty;
                string imprint = string.Empty;
                string collation = string.Empty;
                string seriesTitle = string.Empty;
                string linkedNotes = string.Empty;
                string subject = string.Empty;
                string accessLevel = "Unrestricted";
                int succeed = 0;
                int failed = 0;

                foreach (LibraryBIB l in result)
                {
                    if (l.UpdatedDate > DateTime.Now.AddDays(-5))
                    {

                        imagePath = DownloadFile(l.BookCoverMedium);

                        if (l.Country == "||") l.Country = null;


                        //***   
                        Title = (!string.IsNullOrEmpty(l.Title) ? l.Title : "")
                            + (!string.IsNullOrEmpty(l.Medium) ? " " + l.Medium : "")
                            + (!string.IsNullOrEmpty(l.RemainderofTitle) ? " : " + l.RemainderofTitle : "")
                            + (!string.IsNullOrEmpty(l.StatementofResponsibility) ? " / " + l.StatementofResponsibility : "");



                        personalAuthor = (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? l.MainEntryPersonalName : "")
                                        + (!string.IsNullOrEmpty(l.AddedEntry_PersonalName) ? (!string.IsNullOrEmpty(l.MainEntryPersonalName) ? "||" : "") + l.AddedEntry_PersonalName : "");

                        corporateAuthor = (!string.IsNullOrEmpty(l.MainEntryCorporateName) ? l.MainEntryCorporateName + "||" : "")
                                      + (!string.IsNullOrEmpty(l.AddedEntry_CorporateName) ? l.AddedEntry_CorporateName : "");



                        //***
                        Artist = (!string.IsNullOrEmpty(personalAuthor) ? personalAuthor : "")
                                     + (!string.IsNullOrEmpty(corporateAuthor) ? (!string.IsNullOrEmpty(personalAuthor) ? "||" : "") + corporateAuthor : "");



                        //***
                        meetingName = (!string.IsNullOrEmpty(l.MeetingName) ? l.MeetingName : "")
                                     + (!string.IsNullOrEmpty(l.AddedEntry_MeetingName) ? (!string.IsNullOrEmpty(l.MeetingName) ? "||" : "") + l.AddedEntry_MeetingName : "");



                        imprint = (!string.IsNullOrEmpty(l.PlaceofPublication)) ? l.PlaceofPublication + " : " + l.NameofPublisher + ", " + l.DateofPublication : " ";



                        collation = (!string.IsNullOrEmpty(l.Collation_Extent)) ? l.Collation_Extent + " : " + l.Collation_OtherPhysicalDetails + " ; " + l.Collation_Dimensions + "  " + l.Collation_Accompanying : "";


                        //***
                        seriesTitle = (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? l.SeriesStatement_Title : "")
                                     + (!string.IsNullOrEmpty(l.SeriesStatement_Volumenumber) ? (!string.IsNullOrEmpty(l.SeriesStatement_Title) ? " ; " : "") + l.SeriesStatement_Volumenumber : "");

                        //linkedNotes = l.Linkingnotes_PrecedingEntry + l.Linkingnotes_SucceedingEntry;
                        //***
                        linkedNotes = (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? l.Linkingnotes_PrecedingEntry : "")
                                     + (!string.IsNullOrEmpty(l.Linkingnotes_SucceedingEntry) ? (!string.IsNullOrEmpty(l.Linkingnotes_PrecedingEntry) ? "||" : "") + l.Linkingnotes_SucceedingEntry : "");



                        subject = (!string.IsNullOrEmpty(l.SubjectAddedEntry_TopicalTerm) ? l.SubjectAddedEntry_TopicalTerm + "||" : "")
                            + (!string.IsNullOrEmpty(l.SubjectAddedEntry_PersonalName) ? l.SubjectAddedEntry_PersonalName + "||" : "")
                            + (!string.IsNullOrEmpty(l.SubjectAddedEntry_CorporateName) ? l.SubjectAddedEntry_CorporateName + "||" : "")
                            + (!string.IsNullOrEmpty(l.SubjectAddedEntry_MeetingName) ? l.SubjectAddedEntry_MeetingName + "||" : "");

                        if (!String.IsNullOrEmpty(l.Title) && !String.IsNullOrEmpty(l.BRN))
                        {

                            int libResult = DBService.LibraryInsert(null, imagePath, Title, Artist, l.DateofPublication,
                                l.Country, accessLevel, l.BRN, personalAuthor, corporateAuthor, meetingName,
                                l.EditionStatement, imprint, collation, seriesTitle, l.GeneralNote, linkedNotes,
                                l.CreationorProductionCreditsNote, l.ParticipantorPerformerNote, l.SystemDetailsNote, l.FormattedContentsNote, l.Summary, l.ISBN, l.ISSN,
                                l.DeweyClass, l.Language, l.VaryingformofTitle, l.RemainderofTitle2, subject, l.DateofPublication,
                                null, l.IRN, l.HostItemEntry, l.CreatedDate, l.UpdatedDate);

                            if (libResult > 0) succeed += 1;
                            else failed += 1;

                        }
                        else
                        {
                            LogService.CreateFailImportLog("No Title & No BRN");
                            failed += 1;
                        }
                    }




                }

                #region Sync Log
                DBService.SyncLogsInsert("LIB", result.Count(), succeed, failed);
                #endregion

                #region Old File Delete
                string[] files = Directory.GetFiles(libPath);

                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);

                    if (fi.LastAccessTime < DateTime.Now.AddDays(-15))
                    {
                        fi.Delete();
                    }
                }
                #endregion


            }
            catch (FileNotFoundException e)
            {
                LogService.CreateErrorLog("LIB Biblio file is not found. Detail error : " + e.Message);

            }
        }

        public static async Task ImportLibraryItemDataFromCivicaApi(string option, string fromDate, string toDate, string irn)
        {
            int dur = 30;
            string dt = DateTime.Now.AddMinutes(0 - dur).ToString("dd/MM/yyyy HH:mm");
            SyncResponse res = new SyncResponse();
            try
            {
                LogService.CreateImportActivityLog("Start sync ImportLibraryItemDataFromCivicaApi: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
                res.status = true;
                if (string.IsNullOrEmpty(option))
                {
                    //get sync history failure
                    spGetDataSyncCivicaFailure_Result[] listSyncFailure = DBService.GetDataSyncCivicaFailure("item");
                    if (listSyncFailure != null && listSyncFailure.Length > 0)
                    {
                        for (int i = 0; i < listSyncFailure.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(listSyncFailure[i].Dt))
                            {
                                await SyncItems(listSyncFailure[i].Id, listSyncFailure[i].Dt, listSyncFailure[i].Dur.HasValue ? listSyncFailure[i].Dur.Value : dur);
                            }
                        }
                    }
                    await SyncItems(0, dt, dur);
                }
                else
                {
                    if (option == "1")
                    {
                        DateTime temp;
                        DateTime fromDateTime;
                        DateTime toDateTime;
                        if (DateTime.TryParse(fromDate, out temp) && DateTime.TryParse(toDate, out temp))
                        {
                            fromDateTime = DateTime.Parse(fromDate);
                            toDateTime = DateTime.Parse(toDate);
                            int countDay = toDateTime.Subtract(fromDateTime).Days;
                            for (int i = 0; i < countDay; i++)
                            {
                                await SyncItems(0, fromDateTime.AddDays(i).ToString("dd/MM/yyyy HH:mm"), 1440);
                            }
                        }
                    }
                    else if (option == "2")
                    {
                        if (!string.IsNullOrEmpty(irn))
                        {
                            await SyncItemById(irn);
                        }
                    }
                }
                LogService.CreateImportActivityLog("End sync ImportLibraryItemDataFromCivicaApi: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
            }
            catch (Exception ex)
            {
                LogService.CreateImportActivityLog("End sync ImportLibraryItemDataFromCivicaApi: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
                LogService.CreateFailImportLog("Import Library Item Data From Civica Api failure: " + "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message);
                res.status = false;
                res.message = "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message;
                UpdateSyncCivicaHistory(0, "item", dt, res.message, dur, res.status);
            }
        }

        public static async Task<SyncResponse> SyncItems(long idSync, string dt, int dur)
        {
            SyncResponse res = new SyncResponse();
            try
            {
                res.status = true;
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "password" },
                    { "username", civicaUsername },
                    { "password", civicaPassword }
                };
                var content = new FormUrlEncodedContent(values);
                var loginResponse = await client.PostAsync(civicaApi + "/ngapi/token", content);
                var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
                LoginCivicaResponse loginRes = JsonConvert.DeserializeObject<LoginCivicaResponse>(loginResponseString);
                if (loginRes != null && !string.IsNullOrEmpty(loginRes.access_token))
                {
                    string token = loginRes.access_token;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var itemListResponse = await client.PostAsync(civicaApi + "/ngapi/item/list?dt=" + dt + "&dur=" + dur.ToString(), null);
                    var itemListResponseString = await itemListResponse.Content.ReadAsStringAsync();
                    SyncItemListData itemRes = JsonConvert.DeserializeObject<SyncItemListData>(itemListResponseString);
                    if (itemRes.status == "SUCCESS")
                    {
                        if (itemRes.data != null && itemRes.data.Count() > 0)
                        {
                            var dataFailure = new List<Item>();
                            var dataSuccess = new List<Item>();
                            int countSuccess = 0;
                            foreach (var itemItem in itemRes.data)
                            {
                                bool isSuccess = false;
                                var itemDetailResponse = await client.GetAsync(civicaApi + "/ngapi/item/" + itemItem.irn.ToString());
                                var itemDetailResponseString = await itemDetailResponse.Content.ReadAsStringAsync();
                                SyncItemDetailData item = JsonConvert.DeserializeObject<SyncItemDetailData>(itemDetailResponseString);
                                if (item.status == "SUCCESS")
                                {
                                    if(item.data != null)
                                    {
                                        int resSync = DBService.LibraryItemsSyncData(item.data);
                                        if (resSync > 0)
                                        {
                                            countSuccess = countSuccess + 1;
                                            isSuccess = true;
                                        }
                                    }
                                    else
                                    {
                                        countSuccess = countSuccess + 1;
                                        isSuccess = true;
                                    }
                                }
                                if (!isSuccess)
                                {
                                    dataFailure.Add(itemItem);
                                }
                                else
                                {
                                    dataSuccess.Add(itemItem);
                                }
                            }
                            res.message = "Data sync is finished."
                                          + "- Total data sync:" + itemRes.data.Count().ToString()
                                          + "- Total data sync successfully:" + countSuccess.ToString()
                                          + "- Total data sync failure: " + (itemRes.data.Count() - countSuccess).ToString();

                            string str = "Data SyncItems is finished."
                                          + "- Dt: " + dt
                                          + "- Dur: " + dur.ToString()
                                          + "- List data sync successfully: " + JsonConvert.SerializeObject(dataSuccess)
                                          + "- List data sync failure: " + JsonConvert.SerializeObject(dataFailure);
                            LogService.CreateImportActivityLog(str);
                            
                            if (itemRes.data.Count() - countSuccess > 0)
                            {
                                res.status = false;
                            }
                        }
                        else
                        {
                            res.message = "No records need to be updated";
                        }
                    }
                    else if (itemRes.status == "FAIL" && itemRes.msg == "Data not found")
                    {
                        res.message = "No records need to be updated";
                    }
                    else
                    {
                        res.status = false;
                        res.message = "Get data Item list failure." + itemRes.msg;
                    }
                }
                else
                {
                    res.status = false;
                    res.message = "Login failure.";
                }
                UpdateSyncCivicaHistory(idSync, "item", dt, res.message, dur, res.status);
                return res;
            }
            catch (Exception ex)
            {
                LogService.CreateFailImportLog("Sync Items: " + "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message);
                res.status = false;
                res.message = "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message;
                UpdateSyncCivicaHistory(idSync, "item", dt, res.message, dur, res.status);
                return res;
            }
        }

        public static async Task<SyncResponse> SyncItemById(string irn)
        {
            SyncResponse res = new SyncResponse();
            try
            {
                res.status = true;
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "password" },
                    { "username", civicaUsername },
                    { "password", civicaPassword }
                };
                var content = new FormUrlEncodedContent(values);
                var loginResponse = await client.PostAsync(civicaApi + "/ngapi/token", content);
                var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
                LoginCivicaResponse loginRes = JsonConvert.DeserializeObject<LoginCivicaResponse>(loginResponseString);
                if (loginRes != null && !string.IsNullOrEmpty(loginRes.access_token))
                {
                    string token = loginRes.access_token;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    int countSuccess = 0;
                    var itemDetailResponse = await client.GetAsync(civicaApi + "/ngapi/item/" + irn.ToString());
                    var itemDetailResponseString = await itemDetailResponse.Content.ReadAsStringAsync();
                    SyncItemDetailData item = JsonConvert.DeserializeObject<SyncItemDetailData>(itemDetailResponseString);
                    if (item.status == "SUCCESS" && item.data != null)
                    {
                        int resSync = DBService.LibraryItemsSyncData(item.data);
                        if (resSync > 0)
                        {
                            countSuccess = countSuccess + 1;
                        }
                        res.message = "Data sync is finished."
                                      + "- Total data sync: 1"
                                      + "- Total data sync successfully:" + countSuccess.ToString()
                                      + "- Total data sync failure: " + (1 - countSuccess).ToString()
                                      + "- List data sync: " + JsonConvert.SerializeObject(item.data);
                    }
                    else
                    {
                        res.message = "Data sync is finished."
                                      + "- No records need to be updated for irn: " + irn;
                    }
                }
                else
                {
                    res.status = false;
                    res.message = "Login failure.";
                }
                UpdateSyncCivicaHistory(0, "item", irn, res.message, 0, res.status);
                return res;
            }
            catch (Exception ex)
            {
                LogService.CreateFailImportLog("Sync Items: " + "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message);
                res.status = false;
                res.message = "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message;
                UpdateSyncCivicaHistory(0, "item", irn, res.message, 0, res.status);
                return res;
            }
        }

        public static void UpdateSyncCivicaHistory(long id, string type, string dt, string message, int dur, bool status)
        {
            try
            {
                SyncCivica hisSync = new SyncCivica();
                hisSync.Id = id;
                hisSync.TypeSync = type;
                hisSync.Dt = dt;
                hisSync.Dur = dur;
                hisSync.Message = message;
                hisSync.Status = status;
                hisSync.StatusUpdate = status;
                hisSync.UpdateTime = DateTime.Now;
                DBService.UpdateSyncCivicaHistory(hisSync);
            }
            catch (Exception ex)
            {
                LogService.CreateFailImportLog("Update Sync Civica History failure: " + "Message: " + ex.Message + " InnerException: " + ex.InnerException.Message);
                throw ex;
            }
        }
    }
}
