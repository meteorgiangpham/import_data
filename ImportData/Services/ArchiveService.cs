﻿using ImportData.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace ImportData
{
    public class ArchiveService
    {      
        
        public static string archive_url = ConfigurationManager.AppSettings["ARC.Api.Url"];
        static string destinationpath = ConfigurationManager.AppSettings["ARC.Download.Path"];
        static string urlForUpdatedData = ConfigurationManager.AppSettings["ARC.Api.UpdateData.Url"];

        private static void SaveArchives(List<ART_ARC> data)
        {
            try
            {
                string imagePath = string.Empty;
                string filePath = string.Empty;
                string dimensions = string.Empty;
                string subject = string.Empty;
                string notes = string.Empty;

                int succeed = 0;
                int failed = 0;

                #region Insert/Update Archive Data
                foreach (ART_ARC r in data.Where(x => x.metadata != null))
                {
                    if (r.derivs.Count() > 3)
                    {
                        imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
                        filePath = DownloadFile(r.derivs[3].url, r.umo_id);
                    }
                    else if (r.derivs.Count() > 0) //PDF // MP3
                    {
                        if (r.thumbnail != null)
                        {
                            imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
                        }
                        else
                        {
                            imagePath = null;
                            LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
                        }

                        //PDF   -> Drive[1] is Compressed PDF  
                        //Video -> Drive[1] is MP4

                        if (r.derivs.Count() > 1)
                            filePath = DownloadFile(r.derivs[1].url, r.umo_id);
                        else
                            filePath = DownloadFile(r.derivs[0].url, r.umo_id);

                        ////if (r.image_type == "VIDEO")

                        ////    filePath = DownloadFile(r.derivs[1].url, r.umo_id);
                        ////else // PDF
                        ////    filePath = DownloadFile(r.derivs[0].url, r.umo_id);
                    }
                    else
                    {
                        LogService.CreateErrorLog("Archives : No derivs For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);

                        if (r.thumbnail != null)
                        {
                            imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
                        }
                        else
                        {
                            imagePath = null;
                            LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
                        }

                        filePath = null;
                    }


                    subject = (!String.IsNullOrEmpty(r.metadata.SUB_PERSONAL_NAME) ? r.metadata.SUB_PERSONAL_NAME + "|" : "")
                            + (!String.IsNullOrEmpty(r.metadata.SUB_PLACE_NAME) ? r.metadata.SUB_PLACE_NAME + "|" : "")
                             + (!String.IsNullOrEmpty(r.metadata.SUB_INSTITUTION_NAME) ? r.metadata.SUB_INSTITUTION_NAME + "|" : "")
                              + (!String.IsNullOrEmpty(r.metadata.SUB_TOPICAL_TERM) ? r.metadata.SUB_TOPICAL_TERM : "");


                    subject = subject.TrimEnd();


                    notes = (!String.IsNullOrEmpty(r.metadata.PUBLICATION_NOTE) ? r.metadata.PUBLICATION_NOTE + ";" : "")
                           + (!String.IsNullOrEmpty(r.metadata.GENERAL_NOTES) ? r.metadata.GENERAL_NOTES + ";" : "")
                            + (!String.IsNullOrEmpty(r.metadata.CONSERVATION_NOTES) ? r.metadata.CONSERVATION_NOTES : "");


                    notes = notes.TrimEnd(';');



                    int result = DBService.ArchiveInsert(r.umo_id, filePath, imagePath, r.metadata.ARC_TITLE, r.metadata.CREATORS, r.metadata.DISPLAY_DATE1, r.metadata.SUB_PLACE_NAME, r.metadata.AccessLevel
                        , r.metadata.REFERENCE_CODE, r.metadata.OTHER_TITLES, r.metadata.FONDS_COLLECTIONS
                        , r.metadata.TYPE_OF_DATE, r.metadata.LANGUAGE, subject, r.metadata.GENERAL_MATERIAL_TYPE, r.metadata.EXTENT_AND_MEDIUM
                        , r.metadata.STORAGE_LOCATION, r.metadata.CONDITIONS_GOVERNING_ACCESS, r.metadata.USAGE_RESTRICTIONS_CODE
                        , r.metadata.CONDITIONS_GOVERNING_USE_AND_REPRODUCTION, r.metadata.SYSTEM_OF_ARRANGEMENT, r.metadata.ARCHIVAL_HISTORY
                        , r.metadata.IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER, r.metadata.EXISTENCE_AND_LOCATION_OF_ORIGINALS, r.metadata.EXISTENCE_AND_LOCATION_OF_COPIES
                        , notes, r.metadata.SCOPE_AND_CONTENT, r.metadata.LEVEL_OF_DETAIL, r.metadata.LEVEL_OF_DESCRIPTION, r.metadata.STATUS, r.metadata.SUB_PERSONAL_NAME);


                    if (result > 0) succeed += 1;
                    else failed += 1;

                }
                #endregion

                #region Sync Log
                DBService.SyncLogsInsert("ARC", data.Where(x => x.metadata != null).Count(), succeed, failed);
                #endregion
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());                
            }         

        }

        public static void ImportArchiveData()
        {
            try
            {
                var auth = AuthService.GetAuth();

                //AUTH auth = new AUTH();
                //auth.SURL = "1098457026ZZXEIWPKUSMT";        

                if (auth != null)
                {
                    var artworkURL = String.Format(archive_url, auth.SURL);


                    ServicePointManager.Expect100Continue = true;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
     

                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
                    int test = request.Timeout;
                    request.Timeout = 3600000;
                    //request.Timeout = 200;
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";                  

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(),encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }
                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

                    dynamic data = JsonConvert.DeserializeObject(myResponse);
                   
                   
                    var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);


                    string imagePath = string.Empty;
                    string filePath = string.Empty;
                    string dimensions = string.Empty;
                    string subject = string.Empty;
                    string notes = string.Empty;

                    int succeed = 0;
                    int failed = 0;

                    foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
                    {
                        if (r.derivs.Count() > 3)
                        {
                            imagePath = DownloadFile(r.derivs[1].url,r.umo_id);
                            filePath = DownloadFile(r.derivs[3].url,r.umo_id);
                        }
                        else if (r.derivs.Count() > 0) //PDF
                        {
                            if (r.thumbnail != null)
                            {
                                imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
                            }
                            else
                            {
                                imagePath = null;
                                LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
                            }


                            filePath = DownloadFile(r.derivs[0].url, r.umo_id);
                        }
                        else
                        {
                            LogService.CreateErrorLog("Archives : No derivs For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);

                            if (r.thumbnail != null)
                            {
                                imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
                            }
                            else
                            {
                                imagePath = null;
                                LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
                            }

                            filePath = null;
                        }


                        subject = (!String.IsNullOrEmpty(r.metadata.SUB_PERSONAL_NAME) ? r.metadata.SUB_PERSONAL_NAME + "|" : "")
                                + (!String.IsNullOrEmpty(r.metadata.SUB_PLACE_NAME) ? r.metadata.SUB_PLACE_NAME + "|" : "")
                                 + (!String.IsNullOrEmpty(r.metadata.SUB_INSTITUTION_NAME) ?  r.metadata.SUB_INSTITUTION_NAME + "|" : "")
                                  + (!String.IsNullOrEmpty(r.metadata.SUB_TOPICAL_TERM) ?  r.metadata.SUB_TOPICAL_TERM : "");


                        subject = subject.TrimEnd();


                        notes = (!String.IsNullOrEmpty(r.metadata.PUBLICATION_NOTE) ? r.metadata.PUBLICATION_NOTE + ";" : "")
                               + (!String.IsNullOrEmpty(r.metadata.GENERAL_NOTES) ? r.metadata.GENERAL_NOTES + ";" : "")
                                + (!String.IsNullOrEmpty(r.metadata.CONSERVATION_NOTES) ? r.metadata.CONSERVATION_NOTES : "");


                        notes = notes.TrimEnd(';');                               



                        int result=DBService.ArchiveInsert(r.umo_id,filePath, imagePath, r.metadata.ARC_TITLE, r.metadata.CREATORS, r.metadata.DISPLAY_DATE1, r.metadata.SUB_PLACE_NAME, r.metadata.AccessLevel
                            , r.metadata.REFERENCE_CODE, r.metadata.OTHER_TITLES, r.metadata.FONDS_COLLECTIONS
                            , r.metadata.TYPE_OF_DATE, r.metadata.LANGUAGE, subject, r.metadata.GENERAL_MATERIAL_TYPE, r.metadata.EXTENT_AND_MEDIUM
                            , r.metadata.STORAGE_LOCATION, r.metadata.CONDITIONS_GOVERNING_ACCESS, r.metadata.USAGE_RESTRICTIONS_CODE
                            , r.metadata.CONDITIONS_GOVERNING_USE_AND_REPRODUCTION, r.metadata.SYSTEM_OF_ARRANGEMENT, r.metadata.ARCHIVAL_HISTORY
                            , r.metadata.IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER, r.metadata.EXISTENCE_AND_LOCATION_OF_ORIGINALS, r.metadata.EXISTENCE_AND_LOCATION_OF_COPIES
                            , notes, r.metadata.SCOPE_AND_CONTENT, r.metadata.LEVEL_OF_DETAIL, r.metadata.LEVEL_OF_DESCRIPTION, r.metadata.STATUS, r.metadata.SUB_PERSONAL_NAME);


                        if (result > 0) succeed += 1;
                        else failed += 1;

                    }

                    #region Sync Log
                    DBService.SyncLogsInsert("ARC", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
                    #endregion


                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }


        public static void ImportArchiveData_ToUpdate()
        {
            try
            {
                var auth = AuthService.GetAuth();


                if (auth != null)
                {
                    string fromDate = DateTime.Now.AddDays(-15).ToString("dd-MM-yyyy");

                    string toDate = DateTime.Now.ToString("dd-MM-yyyy");

                    LogService.CreateImportActivityLog("Import daily archive data  between from date : ( " + fromDate + " )  and to date : ( " + toDate + " )  , current date time :" + DateTime.Now.ToString());


                    var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

                    ServicePointManager.Expect100Continue = true;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
                    int test = request.Timeout;
                    request.Timeout = 3600000;
                    //request.Timeout = 200;
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }                   


                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

                    //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
                    dynamic data = JsonConvert.DeserializeObject(myResponse);
                                      

                    var hasData = data.Count;
                    if (hasData <= 1)
                    {
                        #region Sync Log
                        DBService.SyncLogsInsert("ARC", 0, 0, 0);
                        #endregion

                        return;
                    }


                    var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);

                    SaveArchives(deserialized);

                    #region OLD
                    ////////string imagePath = string.Empty;
                    ////////string filePath = string.Empty;
                    ////////string dimensions = string.Empty;
                    ////////string subject = string.Empty;
                    ////////string notes = string.Empty;

                    ////////int succeed = 0;
                    ////////int failed = 0;

                    ////////#region Insert/Update Archive Data
                    ////////foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
                    ////////{
                    ////////    if (r.derivs.Count() > 3)
                    ////////    {
                    ////////        imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
                    ////////        filePath = DownloadFile(r.derivs[3].url, r.umo_id);
                    ////////    }
                    ////////    else if (r.derivs.Count() > 0) //PDF
                    ////////    {
                    ////////        if (r.thumbnail != null)
                    ////////        {
                    ////////            imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
                    ////////        }
                    ////////        else
                    ////////        {
                    ////////            imagePath = null;
                    ////////            LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
                    ////////        }


                    ////////        filePath = DownloadFile(r.derivs[0].url, r.umo_id);
                    ////////    }
                    ////////    else
                    ////////    {
                    ////////        LogService.CreateErrorLog("Archives : No derivs For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);

                    ////////        if (r.thumbnail != null)
                    ////////        {
                    ////////            imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
                    ////////        }
                    ////////        else
                    ////////        {
                    ////////            imagePath = null;
                    ////////            LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
                    ////////        }

                    ////////        filePath = null;
                    ////////    }


                    ////////    subject = (!String.IsNullOrEmpty(r.metadata.SUB_PERSONAL_NAME) ? r.metadata.SUB_PERSONAL_NAME + "|" : "")
                    ////////            + (!String.IsNullOrEmpty(r.metadata.SUB_PLACE_NAME) ? r.metadata.SUB_PLACE_NAME + "|" : "")
                    ////////             + (!String.IsNullOrEmpty(r.metadata.SUB_INSTITUTION_NAME) ? r.metadata.SUB_INSTITUTION_NAME + "|" : "")
                    ////////              + (!String.IsNullOrEmpty(r.metadata.SUB_TOPICAL_TERM) ? r.metadata.SUB_TOPICAL_TERM : "");


                    ////////    subject = subject.TrimEnd();


                    ////////    notes = (!String.IsNullOrEmpty(r.metadata.PUBLICATION_NOTE) ? r.metadata.PUBLICATION_NOTE + ";" : "")
                    ////////           + (!String.IsNullOrEmpty(r.metadata.GENERAL_NOTES) ? r.metadata.GENERAL_NOTES + ";" : "")
                    ////////            + (!String.IsNullOrEmpty(r.metadata.CONSERVATION_NOTES) ? r.metadata.CONSERVATION_NOTES : "");


                    ////////    notes = notes.TrimEnd(';');



                    ////////    int result = DBService.ArchiveInsert(r.umo_id, filePath, imagePath, r.metadata.ARC_TITLE, r.metadata.CREATORS, r.metadata.DISPLAY_DATE1, r.metadata.SUB_PLACE_NAME, r.metadata.AccessLevel
                    ////////        , r.metadata.REFERENCE_CODE, r.metadata.OTHER_TITLES, r.metadata.FONDS_COLLECTIONS
                    ////////        , r.metadata.TYPE_OF_DATE, r.metadata.LANGUAGE, subject, r.metadata.GENERAL_MATERIAL_TYPE, r.metadata.EXTENT_AND_MEDIUM
                    ////////        , r.metadata.STORAGE_LOCATION, r.metadata.CONDITIONS_GOVERNING_ACCESS, r.metadata.USAGE_RESTRICTIONS_CODE
                    ////////        , r.metadata.CONDITIONS_GOVERNING_USE_AND_REPRODUCTION, r.metadata.SYSTEM_OF_ARRANGEMENT, r.metadata.ARCHIVAL_HISTORY
                    ////////        , r.metadata.IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER, r.metadata.EXISTENCE_AND_LOCATION_OF_ORIGINALS, r.metadata.EXISTENCE_AND_LOCATION_OF_COPIES
                    ////////        , notes, r.metadata.SCOPE_AND_CONTENT, r.metadata.LEVEL_OF_DETAIL, r.metadata.LEVEL_OF_DESCRIPTION, r.metadata.STATUS, r.metadata.SUB_PERSONAL_NAME);


                    ////////    if (result > 0) succeed += 1;
                    ////////    else failed += 1;

                    ////////}

                    ////////#region Sync Log
                    ////////DBService.SyncLogsInsert("ARC", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
                    ////////#endregion
                    ////////#endregion

                    #endregion



                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }


        public static void ImportArchiveData_ToUpdate_SpecificDate()
        {
            try
            {
                var auth = AuthService.GetAuth();


                if (auth != null)
                {
                    //string fromDate = DateTime.Now.AddDays(-2).ToString("dd-MM-yyyy");

                    //string toDate = DateTime.Now.ToString("dd-MM-yyyy");

                    string fromDate = "20-04-2020";

                    string toDate = DateTime.Now.ToString("dd-MM-yyyy");

                    LogService.CreateImportActivityLog("Import daily archive data  between from date : ( " + fromDate + " )  and to date : ( " + toDate + " )  , current date time :" + DateTime.Now.ToString());


                    var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

                    ServicePointManager.Expect100Continue = true;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
                    int test = request.Timeout;
                    request.Timeout = 3600000;
                    //request.Timeout = 200;
                    request.Method = "Get";
                    request.KeepAlive = true;
                    request.ContentType = "appication/json";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    string myResponse = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
                    {
                        myResponse = sr.ReadToEnd();
                    }


                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

                    //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
                    dynamic data = JsonConvert.DeserializeObject(myResponse);


                    var hasData = data.Count;
                    if (hasData <= 1)
                    {
                        #region Sync Log
                        DBService.SyncLogsInsert("ARC", 0, 0, 0);
                        #endregion

                        return;
                    }


                    var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);


                    SaveArchives(deserialized);   



                }
            }
            catch (Exception e)
            {
                LogService.CreateErrorLog(e.ToString());
            }

        }



        //////public static void ImportArchiveData_ToUpdateByBackDate()
        //////{
        //////    try
        //////    {
        //////        var auth = AuthService.GetAuth();


        //////        if (auth != null)
        //////        {
        //////            string fromDate = DateTime.Now.AddDays(-7).ToString("dd-MM-yyyy");

        //////            string toDate = DateTime.Now.ToString("dd-MM-yyyy");

        //////            LogService.CreateImportActivityLog("Import weekly back date check archive data  between from date : ( " + fromDate + " )  and to date : ( " + toDate + " )  , current date time :" + DateTime.Now.ToString());


        //////            var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

        //////            ServicePointManager.Expect100Continue = true;
        //////            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //////            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


        //////            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


        //////            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
        //////            int test = request.Timeout;
        //////            request.Timeout = 3600000;
        //////            //request.Timeout = 200;
        //////            request.Method = "Get";
        //////            request.KeepAlive = true;
        //////            request.ContentType = "appication/json";

        //////            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //////            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
        //////            string myResponse = "";
        //////            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
        //////            {
        //////                myResponse = sr.ReadToEnd();
        //////            }


        //////            // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

        //////            //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
        //////            dynamic data = JsonConvert.DeserializeObject(myResponse);


        //////            var hasData = data.Count;
        //////            if (hasData <= 1)
        //////            {
        //////                #region Sync Log
        //////                DBService.SyncLogsInsert("ARC", 0, 0, 0);
        //////                #endregion

        //////                return;
        //////            }


        //////            var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);


        //////            string imagePath = string.Empty;
        //////            string filePath = string.Empty;
        //////            string dimensions = string.Empty;
        //////            string subject = string.Empty;
        //////            string notes = string.Empty;

        //////            int succeed = 0;
        //////            int failed = 0;

        //////            foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
        //////            {
        //////                if (r.derivs.Count() > 3)
        //////                {
        //////                    imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
        //////                    filePath = DownloadFile(r.derivs[3].url, r.umo_id);
        //////                }
        //////                else if (r.derivs.Count() > 0) //PDF
        //////                {
        //////                    if (r.thumbnail != null)
        //////                    {
        //////                        imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
        //////                    }
        //////                    else
        //////                    {
        //////                        imagePath = null;
        //////                        LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
        //////                    }


        //////                    filePath = DownloadFile(r.derivs[0].url, r.umo_id);
        //////                }
        //////                else
        //////                {
        //////                    LogService.CreateErrorLog("Archives : No derivs For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);

        //////                    if (r.thumbnail != null)
        //////                    {
        //////                        imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
        //////                    }
        //////                    else
        //////                    {
        //////                        imagePath = null;
        //////                        LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.ARC_TITLE);
        //////                    }

        //////                    filePath = null;
        //////                }


        //////                subject = (!String.IsNullOrEmpty(r.metadata.SUB_PERSONAL_NAME) ? r.metadata.SUB_PERSONAL_NAME + "|" : "")
        //////                        + (!String.IsNullOrEmpty(r.metadata.SUB_PLACE_NAME) ? r.metadata.SUB_PLACE_NAME + "|" : "")
        //////                         + (!String.IsNullOrEmpty(r.metadata.SUB_INSTITUTION_NAME) ? r.metadata.SUB_INSTITUTION_NAME + "|" : "")
        //////                          + (!String.IsNullOrEmpty(r.metadata.SUB_TOPICAL_TERM) ? r.metadata.SUB_TOPICAL_TERM : "");


        //////                subject = subject.TrimEnd();


        //////                notes = (!String.IsNullOrEmpty(r.metadata.PUBLICATION_NOTE) ? r.metadata.PUBLICATION_NOTE + ";" : "")
        //////                       + (!String.IsNullOrEmpty(r.metadata.GENERAL_NOTES) ? r.metadata.GENERAL_NOTES + ";" : "")
        //////                        + (!String.IsNullOrEmpty(r.metadata.CONSERVATION_NOTES) ? r.metadata.CONSERVATION_NOTES : "");


        //////                notes = notes.TrimEnd(';');



        //////                int result = DBService.ArchiveInsert(r.umo_id, filePath, imagePath, r.metadata.ARC_TITLE, r.metadata.CREATORS, r.metadata.DISPLAY_DATE1, r.metadata.SUB_PLACE_NAME, r.metadata.AccessLevel
        //////                    , r.metadata.REFERENCE_CODE, r.metadata.OTHER_TITLES, r.metadata.FONDS_COLLECTIONS
        //////                    , r.metadata.TYPE_OF_DATE, r.metadata.LANGUAGE, subject, r.metadata.GENERAL_MATERIAL_TYPE, r.metadata.EXTENT_AND_MEDIUM
        //////                    , r.metadata.STORAGE_LOCATION, r.metadata.CONDITIONS_GOVERNING_ACCESS, r.metadata.USAGE_RESTRICTIONS_CODE
        //////                    , r.metadata.CONDITIONS_GOVERNING_USE_AND_REPRODUCTION, r.metadata.SYSTEM_OF_ARRANGEMENT, r.metadata.ARCHIVAL_HISTORY
        //////                    , r.metadata.IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER, r.metadata.EXISTENCE_AND_LOCATION_OF_ORIGINALS, r.metadata.EXISTENCE_AND_LOCATION_OF_COPIES
        //////                    , notes, r.metadata.SCOPE_AND_CONTENT, r.metadata.LEVEL_OF_DETAIL, r.metadata.LEVEL_OF_DESCRIPTION, r.metadata.STATUS,r.metadata.SUB_PERSONAL_NAME);


        //////                if (result > 0) succeed += 1;
        //////                else failed += 1;

        //////            }

        //////            #region Sync Log
        //////            DBService.SyncLogsInsert("ARC", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
        //////            #endregion



        //////        }
        //////    }
        //////    catch (Exception e)
        //////    {
        //////        LogService.CreateErrorLog(e.ToString());
        //////    }

        //////}

        //public static void ImportArchiveData_ToUpdateByCheckDate(string startDate,string endDate)
        //{
        //    try
        //    {
        //        var auth = AuthService.GetAuth();


        //        if (auth != null)
        //        {
        //            string fromDate = Convert.ToDateTime(startDate).ToString("dd-MM-yyyy");

        //            string toDate = Convert.ToDateTime(endDate).ToString("dd-MM-yyyy");


        //            var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

        //            ServicePointManager.Expect100Continue = true;
        //            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


        //            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
        //            int test = request.Timeout;
        //            request.Timeout = 3600000;
        //            //request.Timeout = 200;
        //            request.Method = "Get";
        //            request.KeepAlive = true;
        //            request.ContentType = "appication/json";

        //            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
        //            string myResponse = "";
        //            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
        //            {
        //                myResponse = sr.ReadToEnd();
        //            }


        //            // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

        //            //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
        //            dynamic data = JsonConvert.DeserializeObject(myResponse);


        //            var hasData = data.Count;
        //            if (hasData <= 1)
        //            {
        //                #region Sync Log
        //                DBService.SyncLogsInsert("ARC", 0, 0, 0);
        //                #endregion

        //                return;
        //            }


        //            var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);


        //            string imagePath = string.Empty;
        //            string filePath = string.Empty;
        //            string dimensions = string.Empty;
        //            string subject = string.Empty;
        //            string notes = string.Empty;

        //            int succeed = 0;
        //            int failed = 0;

        //            foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
        //            {
        //                if (r.derivs.Count() > 3)
        //                {
        //                    imagePath = DownloadFile(r.derivs[1].url);
        //                    filePath = DownloadFile(r.derivs[3].url);
        //                }
        //                else if (r.derivs.Count() > 0) //PDF
        //                {
        //                    if (r.thumbnail != null)
        //                    {
        //                        imagePath = DownloadFile(r.thumbnail[0].url);
        //                    }
        //                    else
        //                    {
        //                        imagePath = null;
        //                        LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.TITLE);
        //                    }


        //                    filePath = DownloadFile(r.derivs[0].url);
        //                }
        //                else
        //                {
        //                    LogService.CreateErrorLog("Archives : No derivs For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.TITLE);

        //                    if (r.thumbnail != null)
        //                    {
        //                        imagePath = DownloadFile(r.thumbnail[0].url);
        //                    }
        //                    else
        //                    {
        //                        imagePath = null;
        //                        LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.TITLE);
        //                    }

        //                    filePath = null;
        //                }


        //                subject = (!String.IsNullOrEmpty(r.metadata.SUB_PERSONAL_NAME) ? r.metadata.SUB_PERSONAL_NAME + "|" : "")
        //                        + (!String.IsNullOrEmpty(r.metadata.SUB_PLACE_NAME) ? r.metadata.SUB_PLACE_NAME + "|" : "")
        //                         + (!String.IsNullOrEmpty(r.metadata.SUB_INSTITUTION_NAME) ? r.metadata.SUB_INSTITUTION_NAME + "|" : "")
        //                          + (!String.IsNullOrEmpty(r.metadata.SUB_TOPICAL_TERM) ? r.metadata.SUB_TOPICAL_TERM : "");


        //                subject = subject.TrimEnd();


        //                notes = (!String.IsNullOrEmpty(r.metadata.PUBLICATION_NOTE) ? r.metadata.PUBLICATION_NOTE + ";" : "")
        //                       + (!String.IsNullOrEmpty(r.metadata.GENERAL_NOTES) ? r.metadata.GENERAL_NOTES + ";" : "")
        //                        + (!String.IsNullOrEmpty(r.metadata.CONSERVATION_NOTES) ? r.metadata.CONSERVATION_NOTES : "");


        //                notes = notes.TrimEnd(';');



        //                int result = DBService.ArchiveInsert(r.umo_id, filePath, imagePath, r.metadata.ARC_TITLE, r.metadata.CREATORS, r.metadata.DISPLAY_DATE1, r.metadata.SUB_PLACE_NAME, r.metadata.AccessLevel
        //                    , r.metadata.REFERENCE_CODE, r.metadata.OTHER_TITLES, r.metadata.FONDS_COLLECTIONS
        //                    , r.metadata.TYPE_OF_DATE, r.metadata.LANGUAGE, subject, r.metadata.GENERAL_MATERIAL_TYPE, r.metadata.EXTENT_AND_MEDIUM
        //                    , r.metadata.STORAGE_LOCATION, r.metadata.CONDITIONS_GOVERNING_ACCESS, r.metadata.USAGE_RESTRICTIONS_CODE
        //                    , r.metadata.CONDITIONS_GOVERNING_USE_AND_REPRODUCTION, r.metadata.SYSTEM_OF_ARRANGEMENT, r.metadata.ARCHIVAL_HISTORY
        //                    , r.metadata.IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER, r.metadata.EXISTENCE_AND_LOCATION_OF_ORIGINALS, r.metadata.EXISTENCE_AND_LOCATION_OF_COPIES
        //                    , notes, r.metadata.SCOPE_AND_CONTENT, r.metadata.LEVEL_OF_DETAIL, r.metadata.LEVEL_OF_DESCRIPTION, r.metadata.STATUS);


        //                if (result > 0) succeed += 1;
        //                else failed += 1;

        //            }

        //            #region Sync Log
        //            DBService.SyncLogsInsert("ARC", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
        //            #endregion



        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LogService.CreateErrorLog(e.ToString());
        //    }

        //}


        /// <summary>
        /// 7 days check back to make sure 
        /// </summary>
        //public static void ImportArchiveData_ToUpdateByBackDate()
        //{
        //    try
        //    {
        //        var auth = AuthService.GetAuth();


        //        if (auth != null)
        //        {
        //            string fromDate = DateTime.Now.AddDays(-7).ToString("dd-MM-yyyy");

        //            string toDate = DateTime.Now.ToString("dd-MM-yyyy");


        //            var artworkURL = String.Format(urlForUpdatedData, auth.SURL, fromDate, toDate);

        //            ServicePointManager.Expect100Continue = true;
        //            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


        //            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(artworkURL);//
        //            int test = request.Timeout;
        //            request.Timeout = 3600000;
        //            //request.Timeout = 200;
        //            request.Method = "Get";
        //            request.KeepAlive = true;
        //            request.ContentType = "appication/json";

        //            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
        //            string myResponse = "";
        //            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), encode))
        //            {
        //                myResponse = sr.ReadToEnd();
        //            }


        //            // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)                    

        //            //[{"call_return":{ "error" : {"root_cause":[{"type":"","reason":""}]},"status" : 0}}
        //            dynamic data = JsonConvert.DeserializeObject(myResponse);

        //            var hasData = data.Count;
        //            if (hasData <= 1)
        //            {
        //                #region Sync Log
        //                DBService.SyncLogsInsert("ARC", 0, 0, 0);
        //                #endregion

        //                return;
        //            }


        //            var deserialized = JsonConvert.DeserializeObject<List<ART_ARC>>(myResponse);


        //            string imagePath = string.Empty;
        //            string filePath = string.Empty;
        //            string dimensions = string.Empty;
        //            string subject = string.Empty;
        //            string notes = string.Empty;

        //            int succeed = 0;
        //            int failed = 0;

        //            Product[] products = DBService.GetProduct("ARC");

        //            foreach (ART_ARC r in deserialized.Where(x => x.metadata != null))
        //            {
        //                DateTime? productDate = products.Where(x => x.Product_UMO_Id == r.umo_id).Select(x => x.Product_ModifiedDate).FirstOrDefault();

        //                if (r.UpdatedDate > productDate)
        //                {
        //                    if (r.derivs.Count() > 3)
        //                    {
        //                        imagePath = DownloadFile(r.derivs[1].url, r.umo_id);
        //                        filePath = DownloadFile(r.derivs[3].url, r.umo_id);
        //                    }
        //                    else if (r.derivs.Count() > 0) //PDF
        //                    {
        //                        if (r.thumbnail != null)
        //                        {
        //                            imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
        //                        }
        //                        else
        //                        {
        //                            imagePath = null;
        //                            LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.TITLE);
        //                        }


        //                        filePath = DownloadFile(r.derivs[0].url,r.umo_id);
        //                    }
        //                    else
        //                    {
        //                        LogService.CreateErrorLog("Archives : No derivs For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.TITLE);

        //                        if (r.thumbnail != null)
        //                        {
        //                            imagePath = DownloadFile(r.thumbnail[0].url, r.umo_id);
        //                        }
        //                        else
        //                        {
        //                            imagePath = null;
        //                            LogService.CreateErrorLog("Archives : No thumbnail For Record UMO ID :" + r.umo_id + " ,REFERENCE_CODE :  " + r.metadata.REFERENCE_CODE + ", Title :" + r.metadata.TITLE);
        //                        }

        //                        filePath = null;
        //                    }


        //                    subject = (!String.IsNullOrEmpty(r.metadata.SUB_PERSONAL_NAME) ? r.metadata.SUB_PERSONAL_NAME + "|" : "")
        //                            + (!String.IsNullOrEmpty(r.metadata.SUB_PLACE_NAME) ? r.metadata.SUB_PLACE_NAME + "|" : "")
        //                             + (!String.IsNullOrEmpty(r.metadata.SUB_INSTITUTION_NAME) ? r.metadata.SUB_INSTITUTION_NAME + "|" : "")
        //                              + (!String.IsNullOrEmpty(r.metadata.SUB_TOPICAL_TERM) ? r.metadata.SUB_TOPICAL_TERM : "");


        //                    subject = subject.TrimEnd();


        //                    notes = (!String.IsNullOrEmpty(r.metadata.PUBLICATION_NOTE) ? r.metadata.PUBLICATION_NOTE + ";" : "")
        //                           + (!String.IsNullOrEmpty(r.metadata.GENERAL_NOTES) ? r.metadata.GENERAL_NOTES + ";" : "")
        //                            + (!String.IsNullOrEmpty(r.metadata.CONSERVATION_NOTES) ? r.metadata.CONSERVATION_NOTES : "");


        //                    notes = notes.TrimEnd(';');



        //                    int result = DBService.ArchiveInsert(r.umo_id, filePath, imagePath, r.metadata.ARC_TITLE, r.metadata.CREATORS, r.metadata.DISPLAY_DATE1, r.metadata.SUB_PLACE_NAME, r.metadata.AccessLevel
        //                        , r.metadata.REFERENCE_CODE, r.metadata.OTHER_TITLES, r.metadata.FONDS_COLLECTIONS
        //                        , r.metadata.TYPE_OF_DATE, r.metadata.LANGUAGE, subject, r.metadata.GENERAL_MATERIAL_TYPE, r.metadata.EXTENT_AND_MEDIUM
        //                        , r.metadata.STORAGE_LOCATION, r.metadata.CONDITIONS_GOVERNING_ACCESS, r.metadata.USAGE_RESTRICTIONS_CODE
        //                        , r.metadata.CONDITIONS_GOVERNING_USE_AND_REPRODUCTION, r.metadata.SYSTEM_OF_ARRANGEMENT, r.metadata.ARCHIVAL_HISTORY
        //                        , r.metadata.IMMEDIATE_SOURCE_OF_ACQUISITION_OR_TRANSFER, r.metadata.EXISTENCE_AND_LOCATION_OF_ORIGINALS, r.metadata.EXISTENCE_AND_LOCATION_OF_COPIES
        //                        , notes, r.metadata.SCOPE_AND_CONTENT, r.metadata.LEVEL_OF_DETAIL, r.metadata.LEVEL_OF_DESCRIPTION, r.metadata.STATUS);


        //                    if (result > 0) succeed += 1;
        //                    else failed += 1;

        //                }
        //            }

        //            #region Sync Log
        //            DBService.SyncLogsInsert("ARC", deserialized.Where(x => x.metadata != null).Count(), succeed, failed);
        //            #endregion



        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LogService.CreateErrorLog(e.ToString());
        //    }

        //}



        private static string DownloadFile(string url, string umoId)
        {
            try {

                //return null;

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                string filename = "";
               
                if (!Directory.Exists(destinationpath))
                {
                    Directory.CreateDirectory(destinationpath);
                }


                using (HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result)
                {
                    string path = response.Headers["Content-Disposition"];
                    if (string.IsNullOrWhiteSpace(path))
                    {
                        var uri = new Uri(url);
                        filename = Path.GetFileName(uri.LocalPath);
                    }
                    else
                    {
                        ContentDisposition contentDisposition = new ContentDisposition(path);
                        filename = contentDisposition.FileName;
                    }

                    var filepath = Path.Combine(destinationpath, filename);
                    //if(File.Exists(filepath))
                    //{                        
                    //    return filename;
                    //}

                    var responseStream = response.GetResponseStream();
                    using (var fileStream = File.Create(System.IO.Path.Combine(destinationpath, filename)))
                    {
                        responseStream.CopyTo(fileStream);
                    }

                    long length = new System.IO.FileInfo(filepath).Length;
                    if (length < 1)
                    {
                        LogService.CreateErrorLog("Archives : No Image/File For Record UMO ID :" + umoId);
                        //Delete from EC2
                        if (File.Exists(filepath))
                        {
                            File.Delete(filepath);
                        }
                        return null;
                    }

                    UploadToS3.UploadFileARC(filepath, filename);

                    //Delete from EC2
                    if (File.Exists(filepath))
                    {
                        File.Delete(filepath);
                    }

                }

                return filename;

               
            }
            catch(Exception e)
            {
                return null;
            }
        }




    }
}
